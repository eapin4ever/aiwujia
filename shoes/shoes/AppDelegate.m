//
//  AppDelegate.m
//  shoes
//
//  Created by pmit on 15/9/9.
//  Copyright (c) 2015年 saygogo. All rights reserved.
//

#import "AppDelegate.h"
#import "WXApi.h"
#import "WeiboSDK.h"
#import "ShoesWebViewController.h"
#import "ShoesUser.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WeiboSDK.h"


@interface AppDelegate () <WXApiDelegate,QQApiInterfaceDelegate,WeiboSDKDelegate,TencentSessionDelegate>


@end

@implementation AppDelegate


//+ (void)initialize
//{
//    //get the original user-agent of webview
//    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
//    NSString *oldAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
//    NSLog(@"old agent :%@", oldAgent);
//    
//    //add my info to the new agent
//    NSString *newAgent = [oldAgent stringByAppendingString:@" myMobileApp"];
//    NSLog(@"new agent :%@", newAgent);
//    
//    //regist the new agent
//    NSDictionary *dictionnary = [[NSDictionary alloc] initWithObjectsAndKeys:newAgent, @"UserAgent", nil];
//    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionnary];
//}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    NSDate *date=[NSDate dateWithTimeIntervalSinceNow:0];
    
    if ([ud objectForKey:@"loginWay"] && ![[ud objectForKey:@"loginWay"] isEqualToString:@""])
    {
        if ([[ud objectForKey:@"loginWay"] isEqualToString:@"qqLogin"])
        {
            
//            NSInteger qqETime = [[ud objectForKey:@"qqExpire"] integerValue];
//            if (time >= qqETime)
//            {
//                
//            }
//            else
//            {
//                TencentOAuth *tencentOAuth = [[TencentOAuth alloc] initWithAppId:qqKey andDelegate:self];
//                tencentOAuth.redirectURI = @"www.qq.com";
//                tencentOAuth.appId = @"1104852644";
//                tencentOAuth.openId = [ud objectForKey:@"qqOpenId"];
//                tencentOAuth.accessToken = [ud objectForKey:@"qqToken"];
//                [tencentOAuth getUserInfo];
//            }
        }
        else if ([[ud objectForKey:@"loginWay"] isEqualToString:@"weixinLogin"])
        {
//            NSInteger weixinETime = [[ud objectForKey:@"weixinTimeCuo"] integerValue];
//            NSInteger weixinLastTime = [[ud objectForKey:@"weixinLastTime"] integerValue];
//            if (time - weixinLastTime < weixinETime)
//            {
//                NSString *weixinToken = [ud objectForKey:@"weixinToken"];
//                NSString *weixinOpenId = [ud objectForKey:@"weixinOpenId"];
//                [self getUserInfoWithToken:weixinToken penId:weixinOpenId];
//            }
        }
        else
        {
            // 打开App 自动登录
            if (([ud objectForKey:@"userAccount"]  && ![[ud objectForKey:@"userAccount"] isEqualToString:@""]) && ([ud objectForKey:@"userPass"]  && ![[ud objectForKey:@"userPass"] isEqualToString:@""]))
            {
                [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusLogin WithParameters:@{@"username":[ud objectForKey:@"userAccount"],@"password":[ud objectForKey:@"userPass"]} callBackBlock:^(NSDictionary *dic) {
                    
                    if ([[dic safeObjectForKey:@"result"] integerValue] == 1)
                    {
                        NSString *key = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"key"];
                        NSString *userName = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"username"];
                        
                        ShoesUser *user = [ShoesUser shareUser];
                        user.key = key;
                        user.userName = userName;
                        
                        
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccess" object:nil];
                        
                    }
                    else
                    {
                        
                    }
                    
                    
                } showIndicator:NO];
            }
            else
            {
                
            }
        }
    }
    
    ShoesWebViewController *shoesWebVC = [[ShoesWebViewController alloc] init];
    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:shoesWebVC];
    self.window.rootViewController = navi;

    
    [WXApi registerApp:wechatKey withDescription:@"aiwujia"];
    [WeiboSDK registerApp:weiboKey];
    
    [self.window makeKeyAndVisible];
    
    TencentOAuth *tencent = [[TencentOAuth alloc] initWithAppId:qqKey andDelegate:self];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - 发送微信请求
- (void)sendAuthRequest
{
    SendAuthReq* req =[[SendAuthReq alloc ] init];
    req.scope = @"snsapi_userinfo,snsapi_base";
    [WXApi sendReq:req];
}

- (void)onResp:(BaseResp *)resp
{
    if ([resp isKindOfClass:[SendAuthResp class]])
    {
        SendAuthResp *aresp = (SendAuthResp *)resp;
        if (aresp.errCode == 0) {
            NSString *code = aresp.code;
            [self getAccess_token:code];
        }
    }
    else if ([resp isKindOfClass:[PayResp class]])
    {
        PayResp *response = (PayResp *)resp;
        if (response.errCode == WXSuccess)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"weixinPaySuccess" object:nil];
        }
        else if (response.errCode == WXErrCodeUserCancel)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"weixinPaySuccess" object:nil];
        }
        else
        {
            NSLog(@"%@ --> %@",@(resp.errCode),resp.errStr);
        }
    }
}

- (void)sendWeixinPeople
{
    NSString *url = [NSString stringWithFormat:@"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%@&secret=%@",wechatKey,wechatSecret];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *zoneUrl = [NSURL URLWithString:url];
        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data) {
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                NSLog(@"responseDic --> %@",dic);
                
            }
        });
    });
}

-(void)getAccess_token:(NSString *)code
{
    //https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
    
    NSString *url =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",wechatKey,wechatSecret,code];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *zoneUrl = [NSURL URLWithString:url];
        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data) {
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                NSString *token = [NSString stringWithFormat:@"%@", [dic objectForKey:@"access_token"]];
                NSString *openId = [NSString stringWithFormat:@"%@", [dic objectForKey:@"openid"]];
                NSInteger timeCuo = [[dic safeObjectForKey:@"expires_in"] integerValue];
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                [ud setValue:@(timeCuo) forKey:@"weixinToken"];
                [ud setValue:openId forKey:@"weixinOpenId"];
                [ud setValue:token forKey:@"weixinToken"];
                
                NSDate *date=[NSDate dateWithTimeIntervalSinceNow:0];
                NSInteger time=[date timeIntervalSince1970];
                [ud setValue:@(time) forKey:@"weixinLastTime"];
                [ud setValue:@(timeCuo) forKey:@"weixinTimeCuo"];
                [ud setValue:@"weixinLogin" forKey:@"loginWay"];
                
                [self getUserInfoWithToken:token penId:openId];
                
            }
        });
    });
}

-(void)getUserInfoWithToken:(NSString *)token penId:(NSString *)openId
{
    // https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID
    
    NSString *url =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",token,openId];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *zoneUrl = [NSURL URLWithString:url];
        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data) {
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                /*
                 {
                 city = Haidian;
                 country = CN;
                 headimgurl = "http://wx.qlogo.cn/mmopen/FrdAUicrPIibcpGzxuD0kjfnvc2klwzQ62a1brlWq1sjNfWREia6W8Cf8kNCbErowsSUcGSIltXTqrhQgPEibYakpl5EokGMibMPU/0";
                 language = "zh_CN";
                 nickname = "xxx";
                 openid = oyAaTjsDx7pl4xxxxxxx;
                 privilege =     (
                 );
                 province = Beijing;
                 sex = 1;
                 unionid = oyAaTjsxxxxxxQ42O3xxxxxxs;
                 }
                 */
                
                NSString *openId = [dic objectForKey:@"openid"];
                NSString *nickName = [dic objectForKey:@"nickname"];
                NSString *headIma = [NSString stringWithFormat:@"%@", [dic objectForKey:@"headimgurl"]];
                NSString *type = @"wechat";
                NSInteger sex = [[dic safeObjectForKey:@"sex"] integerValue];
                NSString *unionString = [dic safeObjectForKey:@"unionid"];
                
                [[NSUserDefaults standardUserDefaults] setValue:openId forKey:@"weixinOpenId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSMutableDictionary *param = [NSMutableDictionary dictionary];
                [param setObject:openId forKey:@"openId"];
                [param setObject:headIma forKey:@"figureUrl"];
                [param setObject:nickName forKey:@"nickName"];
                [param setObject:@(sex) forKey:@"sex"];
                [param setObject:type forKey:@"type"];
                [param setObject:unionString forKey:@"unionid"];
                
                [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusThirdLogin WithParameters:param callBackBlock:^(NSDictionary *loginDic) {
                    if ([[loginDic safeObjectForKey:@"result"] integerValue] == 1)
                    {
                        NSString *key = [[loginDic safeObjectForKey:@"data"] safeObjectForKey:@"key"];
                        NSString *userName = [[loginDic safeObjectForKey:@"data"] safeObjectForKey:@"username"];
                        
                        ShoesUser *user = [ShoesUser shareUser];
                        user.key = key;
                        user.userName = userName;
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccess" object:nil];
                    }
                    
                    
                } showIndicator:NO];
                
            }
        });
        
    });
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
    if ([response isKindOfClass:WBSendMessageToWeiboResponse.class])
    {
        //        WBSendMessageToWeiboResponse *wbResp = (WBSendMessageToWeiboResponse *)response;
        if (response.statusCode == WeiboSDKResponseStatusCodeSuccess)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendWeiboSuccess" object:nil];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendWeiboFailure" object:nil];
        }
        
    }
    else if ([response isKindOfClass:WBAuthorizeResponse.class])
    {
        
        if ([[(WBAuthorizeResponse *)response userID] isKindOfClass:[NSString class]] && ![[(WBAuthorizeResponse *)response userID] isEqualToString:@""])
        {
            self.wbtoken = [(WBAuthorizeResponse *)response accessToken];
            self.wbCurrentUserID = [(WBAuthorizeResponse *)response userID];
            self.wbRefreshToken = [(WBAuthorizeResponse *)response refreshToken];
            [self getWBUsers];
        }
        
    }
    else if ([response isKindOfClass:WBPaymentResponse.class])
    {
        NSString *title = NSLocalizedString(@"支付结果", nil);
        NSString *message = [NSString stringWithFormat:@"%@: %d\nresponse.payStatusCode: %@\nresponse.payStatusMessage: %@\n%@: %@\n%@: %@", NSLocalizedString(@"响应状态", nil), (int)response.statusCode,[(WBPaymentResponse *)response payStatusCode], [(WBPaymentResponse *)response payStatusMessage], NSLocalizedString(@"响应UserInfo数据", nil),response.userInfo, NSLocalizedString(@"原请求UserInfo数据", nil), response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"确定", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if([response isKindOfClass:WBSDKAppRecommendResponse.class])
    {
        NSString *title = NSLocalizedString(@"邀请结果", nil);
        NSString *message = [NSString stringWithFormat:@"accesstoken:\n%@\nresponse.StatusCode: %d\n响应UserInfo数据:%@\n原请求UserInfo数据:%@",[(WBSDKAppRecommendResponse *)response accessToken],(int)response.statusCode,response.userInfo,response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"确定", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)getWBUsers
{
    NSURL *wbUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.weibo.com/2/users/show.json?access_token=%@&uid=%@",self.wbtoken,self.wbCurrentUserID]];
    NSURLRequest *request=[NSURLRequest requestWithURL:wbUrl];
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"wxdic --> %@",responseDic);
        if (![responseDic safeObjectForKey:@"error_code"])
        {
            NSString *avatar = [responseDic objectForKey:@"avatar_hd"];
            NSString *nickName = [responseDic objectForKey:@"name"];
            NSString *platFormType = @"8";
            
            [[NSUserDefaults standardUserDefaults] setValue:self.wbCurrentUserID forKey:@"sinaOpenId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSMutableDictionary *dataDic = [NSMutableDictionary dictionary];
            [dataDic setObject:self.wbCurrentUserID forKey:@"OpenId"];
            [dataDic setObject:avatar forKey:@"Avatar"];
            [dataDic setObject:nickName forKey:@"NickName"];
            [dataDic setObject:platFormType forKey:@"PlatformType"];
        }
        
        
    }];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [TencentOAuth HandleOpenURL:url] || [QQApiInterface handleOpenURL:url delegate:self] || [WXApi handleOpenURL:url delegate:self] || [WeiboSDK handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [TencentOAuth HandleOpenURL:url] || [QQApiInterface handleOpenURL:url delegate:self] || [WXApi handleOpenURL:url delegate:self] || [WeiboSDK handleOpenURL:url delegate:self];
}

- (void)getUserInfoResponse:(APIResponse*) response
{
    //NSLog(@"HHHHHHHHH ==== %@", response.jsonResponse);
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *openId = [ud objectForKey:@"qqOpenId"];
    NSString *avatar = [response.jsonResponse safeObjectForKey:@"figureurl_qq_1"];
    NSString *nickName = [response.jsonResponse safeObjectForKey:@"nickname"];
    NSString *type = @"qq";
    NSString *gender = [response.jsonResponse safeObjectForKey:@"gender"];
    NSInteger sex = 0;
    if (gender && [gender isEqualToString:@"女"])
    {
        sex = 2;
    }
    else if (gender && [gender isEqualToString:@"男"])
    {
        sex = 1;
    }
    else
    {
        sex = 0;
    }
    
    NSLog(@"json --> %@",response.jsonResponse);
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:openId forKey:@"openId"];
    [data setObject:avatar forKey:@"figureUrl"];
    [data setObject:nickName forKey:@"nickName"];
    [data setObject:@(sex) forKey:@"sex"];
    [data setObject:type forKey:@"type"];
    
    [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusThirdLogin WithParameters:data callBackBlock:^(NSDictionary *dic) {
        if ([[dic safeObjectForKey:@"result"] integerValue] == 1)
        {
            NSString *key = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"key"];
            NSString *userName = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"username"];
            
            ShoesUser *user = [ShoesUser shareUser];
            user.key = key;
            user.userName = userName;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccess" object:nil];
        }
        
    } showIndicator:NO];
    
}

@end
