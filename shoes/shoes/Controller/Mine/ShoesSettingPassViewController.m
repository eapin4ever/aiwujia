//
//  ShoesSettingPassViewController.m
//  shoes
//
//  Created by pmit on 15/9/21.
//  Copyright © 2015年 saygogo. All rights reserved.
//

#import "ShoesSettingPassViewController.h"
#import "CustomerTextField.h"
#import "ShoesUser.h"
#import "ShoesMyLoginViewController.h"
#import "PMAiwujiaAgreementViewController.h"

@interface ShoesSettingPassViewController () <UITextFieldDelegate>

@property (strong,nonatomic) UIView *backgroundView;
@property (strong,nonatomic) UIImageView *backgroundIma;
@property (strong,nonatomic) CustomerTextField *passwordTF;
@property (strong,nonatomic) CustomerTextField *rePasswordTF;
@property (strong,nonatomic) UIView *logoView;
@property (strong,nonatomic) UIView *tipView;
@property (strong,nonatomic) UIView *inputView;
@property (strong,nonatomic) UIButton *registerBtn;
@property (strong,nonatomic) UIView *shadowView;

@end

@implementation ShoesSettingPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [self.view addSubview:self.backgroundView];
    
    self.backgroundIma = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.backgroundView.frame.size.width, self.backgroundView.frame.size.height)];
    self.backgroundIma.image = [UIImage imageNamed:@"loginBackground"];
    self.backgroundIma.userInteractionEnabled = YES;
    [self.backgroundView addSubview:self.backgroundIma];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.backgroundView addGestureRecognizer:swipe];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn setImage:[UIImage imageNamed:@"shoesBack"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    [self buildLogoView];
    //[self buildTipView];
    [self buildInputView];
    [self buildNextBtnView];
    [self buildShadowView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyBoard:)];
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildLogoView
{
    self.logoView = [[UIView alloc] initWithFrame:CGRectMake(0, 70, WIDTH, HeightRate(150))];
    self.logoView.backgroundColor = [UIColor clearColor];
    [self.backgroundIma addSubview:self.logoView];
    
    UIImageView *logoIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"shareIcon"]];
    logoIV.contentMode = UIViewContentModeScaleAspectFit;
    logoIV.center = CGPointMake(WIDTH / 2, HeightRate(40));
    logoIV.bounds = (CGRect){CGPointZero,{HeightRate(95),HeightRate(95)}};
    [self.logoView addSubview:logoIV];
    
    UIImageView *logoTitleIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logoTitle"]];
    logoTitleIV.contentMode = UIViewContentModeScaleAspectFit;
    logoTitleIV.center = CGPointMake(WIDTH / 2, HeightRate(105));
    logoTitleIV.bounds = (CGRect){CGPointZero,{WIDTH,HeightRate(30)}};
    [self.logoView addSubview:logoTitleIV];
}

- (void)buildTipView
{
    UIFont *font = [UIFont systemFontOfSize:16.0f];
    
    self.tipView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.logoView.frame) + HeightRate(30), WIDTH, HeightRate(20))];
    self.tipView.backgroundColor = [UIColor whiteColor];
    UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tipView.bounds.size.width, self.tipView.bounds.size.height)];
    tipLB.font = font;
    tipLB.textAlignment = NSTextAlignmentCenter;
    tipLB.textColor = RGBA(243, 91, 36, 1);
    tipLB.text = @"用户注册Step2_设置密码";
    [self.tipView addSubview:tipLB];
    [self.backgroundIma addSubview:self.tipView];
}

- (void)buildInputView
{
    self.inputView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.logoView.frame) + HeightRate(20), WIDTH, HeightRate(145))];
    self.inputView.backgroundColor = [UIColor clearColor];
    [self.backgroundIma addSubview:self.inputView];
    
    UIView *passView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(50), 0, WIDTH - WidthRate(100), HeightRate(65))];
    passView.backgroundColor = [UIColor clearColor];
    [self.inputView addSubview:passView];
    
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(0, passView.frame.size.height - 1, passView.frame.size.width, 1);
    layer1.backgroundColor = RGBA(222, 222, 222, 1).CGColor;
    [passView.layer addSublayer:layer1];
    
    UIImageView *passwordIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(5), HeightRate(15), WidthRate(30), HeightRate(40))];
    passwordIV.image = [UIImage imageNamed:@"password"];
    passwordIV.contentMode = UIViewContentModeScaleAspectFit;
    passwordIV.userInteractionEnabled = YES;
    [passView addSubview:passwordIV];
    
    
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    
    self.passwordTF = [[CustomerTextField alloc] initWithFrame:CGRectMake(WidthRate(50), HeightRate(10), passView.bounds.size.width - WidthRate(50), HeightRate(50))];
    self.passwordTF.placeholder = @"请输入密码";
    [self.passwordTF setValue:RGBA(145, 145, 145, 1) forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordTF setValue:font forKeyPath:@"_placeholderLabel.font"];
    self.passwordTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.passwordTF.delegate = self;
    self.passwordTF.secureTextEntry = YES;
    self.passwordTF.returnKeyType = UIReturnKeyDone;
    [passView addSubview:self.passwordTF];
    
    
    UIView *repassView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(50), CGRectGetMaxY(passView.frame), WIDTH - WidthRate(100), HeightRate(65))];
    repassView.backgroundColor = [UIColor clearColor];
    [self.inputView addSubview:repassView];
    
    CALayer *layer2 = [CALayer layer];
    layer2.frame = CGRectMake(0, repassView.frame.size.height - 1, repassView.frame.size.width, 1);
    layer2.backgroundColor = RGBA(222, 222, 222, 1).CGColor;
    [repassView.layer addSublayer:layer2];
    
    UIImageView *rePasswordIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(5), HeightRate(15), WidthRate(30), HeightRate(40))];
    rePasswordIV.image = [UIImage imageNamed:@"rePassword"];
    rePasswordIV.contentMode = UIViewContentModeScaleAspectFit;
    rePasswordIV.userInteractionEnabled = YES;
    [repassView addSubview:rePasswordIV];
    
    self.rePasswordTF = [[CustomerTextField alloc] initWithFrame:CGRectMake(WidthRate(50), HeightRate(10), WIDTH - WidthRate(100), HeightRate(50))];
    self.rePasswordTF.placeholder = @"请再次确认密码";
    [self.rePasswordTF setValue:RGBA(145, 145, 145, 1) forKeyPath:@"_placeholderLabel.textColor"];
    [self.rePasswordTF setValue:font forKeyPath:@"_placeholderLabel.font"];
    self.rePasswordTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.rePasswordTF.delegate = self;
    self.rePasswordTF.tag = 2;
    self.rePasswordTF.secureTextEntry = YES;
    [repassView addSubview:self.rePasswordTF];
    
}

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)buildShadowView
{
    self.shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.shadowView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.shadowView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.shadowView.bounds.size.width, self.shadowView.bounds.size.height)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.6;
    [self.shadowView addSubview:alphaView];
    
    self.shadowView.hidden = YES;
}

- (void)buildNextBtnView
{
    self.registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.registerBtn.frame = CGRectMake(WidthRate(50), CGRectGetMaxY(self.inputView.frame) + HeightRate(30), WIDTH - WidthRate(100), HeightRate(44));
    [self.registerBtn setTitle:@"注 册" forState:UIControlStateNormal];
    self.registerBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [self.registerBtn addTarget:self action:@selector(goToSettingPass:) forControlEvents:UIControlEventTouchUpInside];
    [self.registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.registerBtn.layer setCornerRadius:3.0f];
    [self.registerBtn.layer setBorderWidth:1.0f];
    [self.registerBtn.layer setBorderColor:RGBA(226, 38, 57, 1).CGColor];
    [self.registerBtn setBackgroundColor:RGBA(228, 48, 90, 1)];
    [self.backgroundIma addSubview:self.registerBtn];
    
    NSString *tipsString = @"点击“注册”按钮，即表示您同意";
    CGSize tipsSize = [tipsString sizeWithFont:[UIFont systemFontOfSize:WidthRate(13)] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
    UILabel *tipsLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(45), CGRectGetMaxY(self.registerBtn.frame) + HeightRate(5), tipsSize.width, HeightRate(20))];
    tipsLB.text = tipsString;
    tipsLB.textColor = RGBA(205, 205, 205, 1);
    tipsLB.font = [UIFont systemFontOfSize:WidthRate(13)];
    tipsLB.textAlignment = NSTextAlignmentLeft;
    [self.backgroundIma addSubview:tipsLB];
    
    NSString *userInstructionsString = @"爱·无价 服务协议";
    CGSize stringSize = [userInstructionsString sizeWithFont:[UIFont systemFontOfSize:WidthRate(13)] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
    
    UILabel *userInstructionsLab = [[UILabel alloc] initWithFrame:CGRectMake(tipsLB.frame.origin.x + tipsLB.frame.size.width + WidthRate(5), tipsLB.frame.origin.y, stringSize.width, HeightRate(20))];
    userInstructionsLab.backgroundColor = [UIColor clearColor];
    userInstructionsLab.text = userInstructionsString;
    userInstructionsLab.textColor = RGBA(205, 205, 205, 1);
    userInstructionsLab.textAlignment = NSTextAlignmentLeft;
    userInstructionsLab.font = [UIFont systemFontOfSize:WidthRate(13)];
    [self.backgroundIma addSubview:userInstructionsLab];
    
    // 分隔线
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(userInstructionsLab.frame.origin.x, tipsLB.frame.origin.y + 18, stringSize.width, 0.5);
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.backgroundIma.layer addSublayer:layer3];
    
    // 手势响应
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userInstructionsAction)];
    [userInstructionsLab addGestureRecognizer:tap];
    userInstructionsLab.userInteractionEnabled = YES;
}

- (void)goToSettingPass:(UIButton *)sender
{
    if (self.isForget)
    {
        [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusFindPass WithParameters:@{@"mobile":self.phoneString,@"verifyCode":self.verCode,@"newPassword":self.passwordTF.text} callBackBlock:^(NSDictionary *dic) {
            
            self.shadowView.hidden = NO;
            [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeRight WaringTitle:@"恭喜\n\n您已修改成功,正在为您登录..." IsBtnShow:NO BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(noway)];
                [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusLogin WithParameters:@{@"username":self.phoneString,@"password":self.passwordTF.text} callBackBlock:^(NSDictionary *dic) {
                    NSString *key = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"key"];
                    NSString *userName = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"username"];
                
                    ShoesUser *user = [ShoesUser shareUser];
                    user.key = key;
                    user.userName = userName;
                
                    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                    [ud setObject:self.phoneString forKey:@"userAccount"];
                    [ud setObject:self.passwordTF.text forKey:@"userPass"];
                    [ud synchronize];
                
                    [NSThread sleepForTimeInterval:2.0f];
                    [self registerSuccess];
                
                
            } showIndicator:NO];
            
        } showIndicator:NO];
    }
    else
    {
        [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusRegister WithParameters:@{@"username":self.phoneString,@"password":self.passwordTF.text,@"password_confirm":self.rePasswordTF.text} callBackBlock:^(NSDictionary *dic) {
            if ([[dic safeObjectForKey:@"result"] integerValue] == 1)
            {
                
                self.shadowView.hidden = NO;
                [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeRight WaringTitle:@"恭喜\n\n您已注册成功,正在为您登录..." IsBtnShow:NO BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(noway)];
                [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusLogin WithParameters:@{@"username":self.phoneString,@"password":self.passwordTF.text} callBackBlock:^(NSDictionary *dic) {
                    NSString *key = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"key"];
                    NSString *userName = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"username"];
                    
                    ShoesUser *user = [ShoesUser shareUser];
                    user.key = key;
                    user.userName = userName;
                    
                    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                    [ud setObject:self.phoneString forKey:@"userAccount"];
                    [ud setObject:self.passwordTF.text forKey:@"userPass"];
                    [ud synchronize];
                    
                    [NSThread sleepForTimeInterval:2.0f];
                    [self registerSuccess];
                    
                    
                } showIndicator:NO];
            }
            else
            {
                NSString *message = [dic safeObjectForKey:@"message"];
                
                self.shadowView.hidden = NO;
                [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWrong WaringTitle:message IsBtnShow:YES BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(alertDismiss)];
            }
            
            
        } showIndicator:NO];
    }
    
}

- (void)registerSuccess
{
    [UIView animateWithDuration:0.3f animations:^{
        
        [[ShoesAlertView shareAlertView] hideTip:self.view];
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        if ([self.navigationController.childViewControllers[0] isKindOfClass:[ShoesMyLoginViewController class]])
        {
            ShoesMyLoginViewController *loginVC = (ShoesMyLoginViewController *)self.navigationController.childViewControllers[0];
            if ([loginVC.loginDelegate respondsToSelector:@selector(loginFinish)])
            {
                [loginVC.loginDelegate loginFinish];
            }
        }
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            
            
        }];
        
    }];
}

- (void)alertDismiss
{
    [UIView animateWithDuration:0.3f animations:^{
        
        [[ShoesAlertView shareAlertView] hideTip:self.view];
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}


- (void)keyBoardWillShow:(NSNotification *)notification
{
    if (self.rePasswordTF.tag == 2) {
        
        NSDictionary *userInfo = [notification userInfo];
        
        NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
        NSTimeInterval animationDuration;
        [animationDurationValue getValue:&animationDuration];
        
        [UIView animateWithDuration:0.3f animations:^{
            
            CGRect rect = self.view.frame;
            if (IS_IOS7 && !IS_IOS8) {
                rect.origin.y = - 80;
            }else{
                rect.origin.y = - 120;
            }
            
            self.view.frame = rect;
            
        }];
    }
}

- (void)keyBoardWillHide:(NSNotification *)notification
{
    if (self.rePasswordTF.tag == 2) {
        
        [UIView beginAnimations:@"myAnimations" context:nil];
        CGRect rect = self.view.frame;
        rect.origin.y = 0;
        self.view.frame = rect;
        [UIView commitAnimations];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
//    [self noWay];
}

- (void)resignKeyBoard:(UITapGestureRecognizer *)tap
{
    [self.passwordTF resignFirstResponder];
    [self.rePasswordTF resignFirstResponder];
}

- (void)noway
{
    
}

#pragma mark - 爱·无价 协议响应事件
- (void)userInstructionsAction
{
    PMAiwujiaAgreementViewController *agreementVC = [[PMAiwujiaAgreementViewController alloc] init];
    [self.navigationController pushViewController:agreementVC animated:YES];
}


@end
