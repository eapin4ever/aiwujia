//
//  ShoesMyLoginViewController.m
//  shoes
//
//  Created by ZhangEapin on 15/9/20.
//  Copyright © 2015年 saygogo. All rights reserved.
//

#import "ShoesMyLoginViewController.h"
#import "CustomerTextField.h"
#import "AppDelegate.h"
#import "WXApi.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import "WeiboSDK.h"
#import "ShoesUser.h"
#import "ShoesMyPhoneVercodeViewController.h"

@interface ShoesMyLoginViewController () <TencentSessionDelegate,UITextFieldDelegate,UIWebViewDelegate>

@property (strong,nonatomic) UIView *backgroundView;
@property (strong,nonatomic) UIImageView *backgroundIma;
@property (strong,nonatomic) CustomerTextField *userNameTF;
@property (strong,nonatomic) CustomerTextField *userPassTF;
@property (strong,nonatomic) UIButton *loginBtn;
@property (strong,nonatomic) UIView *navBackView;
@property (strong,nonatomic) UIView *shadowView;
@property (strong,nonatomic) UIView *logoView;
@property (strong,nonatomic) UIView *inputView;
@property (strong,nonatomic) UIButton *registerBtn;
@property (strong,nonatomic) UIScrollView *contentScroll;
@property (strong,nonatomic) NSArray *thirdLoginIVNameArr;
@property (strong,nonatomic) TencentOAuth *tencentOAuth;
@property (strong,nonatomic) NSArray *permissions;

@property (strong,nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation ShoesMyLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(thirdLoginFinish:) name:@"loginSuccess" object:nil];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [self.view addSubview:self.backgroundView];
    
    self.backgroundIma = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.backgroundView.frame.size.width, self.backgroundView.frame.size.height)];
    self.backgroundIma.image = [UIImage imageNamed:@"loginBackground"];
    self.backgroundIma.userInteractionEnabled = YES;
    [self.backgroundView addSubview:self.backgroundIma];
    
    self.thirdLoginIVNameArr = @[@"shoesQQ",@"shoesWechat",@"shoesWeibo"];
    
    [self buildScroll];
    [self buildLogoView];
    [self buildInputView];
//    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi])
//    {
//        [self buildThirdView];
//    }
    
    [self buildShadowView];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    if (self.isFromLogout)
    {
        self.userNameTF.text = [ud objectForKey:@"userAccount"];
        self.userPassTF.text = [ud objectForKey:@"userPass"];
    }
    
    
//    // Do any additional setup after loading the view, typically from a nib.
//    UIWebView *webView1 = [[UIWebView alloc] initWithFrame:CGRectZero];
//    NSString *Agent = [webView1 stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
//    NSLog(@"Agent === %@", Agent);
//    
//    //add my info to the new agent
//    NSString *newAgent = nil;
//    
//    newAgent = [Agent stringByAppendingString:@"myMobileApp"];
//    NSLog(@"newAgent === %@", newAgent);
//    
//    //regist the new agent
//    NSDictionary *dictionnary = [[NSDictionary alloc] initWithObjectsAndKeys:newAgent, @"UserAgent",nil];
//    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionnary];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [UIApplication sharedApplication].statusBarHidden = YES;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self getBackView:self.navigationController.navigationBar];
    if (![self.userNameTF.text isEqualToString:@""] && ![self.userPassTF.text isEqualToString:@""])
    {
        self.loginBtn.enabled = YES;
    }
}

- (void)buildScroll
{
    self.contentScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, WIDTH, HEIGHT - 44)];
    [self.backgroundIma addSubview:self.contentScroll];
    self.contentScroll.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(regisnAllResponse:)];
    [self.contentScroll addGestureRecognizer:tap];
}

- (void)buildShadowView
{
    self.shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.shadowView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.shadowView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.shadowView.bounds.size.width, self.shadowView.bounds.size.height)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.6;
    [self.shadowView addSubview:alphaView];
    
    self.shadowView.hidden = YES;
}

- (void)buildLogoView
{
    self.logoView = [[UIView alloc] initWithFrame:CGRectMake(0, - HeightRate(40), WIDTH, HeightRate(150))];
    self.logoView.backgroundColor = [UIColor clearColor];
    [self.contentScroll addSubview:self.logoView];
    
    UIImageView *logoIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"shareIcon"]];
    logoIV.contentMode = UIViewContentModeScaleAspectFit;
    logoIV.center = CGPointMake(WIDTH / 2, HeightRate(40));
    logoIV.bounds = (CGRect){CGPointZero,{HeightRate(95),HeightRate(95)}};
    [self.logoView addSubview:logoIV];
    
    UIImageView *logoTitleIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logoTitle"]];
    logoTitleIV.contentMode = UIViewContentModeScaleAspectFit;
    logoTitleIV.center = CGPointMake(WIDTH / 2, HeightRate(105));
    logoTitleIV.bounds = (CGRect){CGPointZero,{WIDTH,HeightRate(30)}};
    [self.logoView addSubview:logoTitleIV];
    
    UIImageView *cancelImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, -10, 20, 20)];
    [cancelImg setImage:[UIImage imageNamed:@"login_cancel"]];
    [self.logoView addSubview:cancelImg];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(15, -10, 30, 30);
    [cancelBtn addTarget:self action:@selector(loginCancel:) forControlEvents:UIControlEventTouchUpInside];
    [self.logoView addSubview:cancelBtn];
}

- (void)buildInputView
{
    self.inputView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.logoView.frame) + HeightRate(10), WIDTH, HeightRate(145))];
    self.inputView.backgroundColor = [UIColor clearColor];
    [self.contentScroll addSubview:self.inputView];
    
    UIView *nameView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(50), 0, WIDTH - WidthRate(100), HeightRate(65))];
    nameView.backgroundColor = [UIColor clearColor];
    [self.inputView addSubview:nameView];
    
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(0, nameView.frame.size.height - 1, nameView.frame.size.width, 1);
    layer1.backgroundColor = RGBA(222, 222, 222, 1).CGColor;
    [nameView.layer addSublayer:layer1];
    
    UIImageView *nameIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(5), HeightRate(15), WidthRate(30), HeightRate(40))];
    nameIV.image = [UIImage imageNamed:@"account"];
    nameIV.contentMode = UIViewContentModeScaleAspectFit;
    nameIV.userInteractionEnabled = YES;
    [nameView addSubview:nameIV];
    
    
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    
    self.userNameTF = [[CustomerTextField alloc] initWithFrame:CGRectMake(WidthRate(60), HeightRate(10), nameView.bounds.size.width - WidthRate(60), HeightRate(50))];
    self.userNameTF.placeholder = @"手机号/用户名";
    [self.userNameTF setValue:RGBA(145, 145, 145, 1) forKeyPath:@"_placeholderLabel.textColor"];
    [self.userNameTF setValue:font forKeyPath:@"_placeholderLabel.font"];
    self.userNameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.userNameTF.delegate = self;
    self.userNameTF.returnKeyType = UIReturnKeyDone;
    [nameView addSubview:self.userNameTF];
    
    
    UIView *passView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(50), CGRectGetMaxY(nameView.frame), WIDTH - WidthRate(100), HeightRate(65))];
    passView.backgroundColor = [UIColor clearColor];
    [self.inputView addSubview:passView];
    
    CALayer *layer2 = [CALayer layer];
    layer2.frame = CGRectMake(0, passView.frame.size.height - 1, passView.frame.size.width, 1);
    layer2.backgroundColor = RGBA(222, 222, 222, 1).CGColor;
    [passView.layer addSublayer:layer2];
    
    UIImageView *passIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(5), HeightRate(15), WidthRate(30), HeightRate(40))];
    passIV.image = [UIImage imageNamed:@"password"];
    passIV.contentMode = UIViewContentModeScaleAspectFit;
    passIV.userInteractionEnabled = YES;
    [passView addSubview:passIV];
    
    self.userPassTF = [[CustomerTextField alloc] initWithFrame:CGRectMake(WidthRate(60), HeightRate(5), passView.bounds.size.width - WidthRate(60), HeightRate(55))];
    self.userPassTF.placeholder = @"请输入登录密码";
    [self.userPassTF setValue:RGBA(145, 145, 145, 1) forKeyPath:@"_placeholderLabel.textColor"];
    [self.userPassTF setValue:font forKeyPath:@"_placeholderLabel.font"];
    self.userPassTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.userPassTF.delegate = self;
    self.userPassTF.secureTextEntry = YES;
    self.userNameTF.returnKeyType = UIReturnKeyDone;
    [passView addSubview:self.userPassTF];
    
    
    self.loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.loginBtn.frame = CGRectMake(WidthRate(50), CGRectGetMaxY(self.inputView.frame) + HeightRate(10), WIDTH - WidthRate(100), HeightRate(44));
    [self.loginBtn setTitle:@"登 录" forState:UIControlStateNormal];
    //[self.loginBtn setBackgroundImage:[UIImage imageNamed:@"loginBg"] forState:UIControlStateNormal];
    self.loginBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [self.loginBtn addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.loginBtn.layer setCornerRadius:3.0f];
    [self.loginBtn.layer setBorderWidth:1.0f];
    [self.loginBtn.layer setBorderColor:RGBA(226, 38, 57, 1).CGColor];
    [self.loginBtn setBackgroundColor:RGBA(228, 48, 90, 1)];
    self.loginBtn.enabled = NO;
    [self.contentScroll addSubview:self.loginBtn];
    
    
    UILabel *tipsLB = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.loginBtn.frame) + HeightRate(5), WIDTH, HeightRate(20))];
    tipsLB.textAlignment = NSTextAlignmentCenter;
    tipsLB.font = [UIFont systemFontOfSize:WidthRate(13)];
    tipsLB.text = @"点击“登录”按钮，即表示您同意 爱·无价 服务协议";
    tipsLB.textColor = RGBA(205, 205, 205, 1);
    [self.contentScroll addSubview:tipsLB];
    
//    self.registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    //self.registerBtn.frame = CGRectMake(WidthRate(60), CGRectGetMaxY(tipsLB.frame) + HeightRate(15), WidthRate(120), HeightRate(20));
//    self.registerBtn.frame = CGRectMake(WidthRate(40), self.contentScroll.frame.size.height - HeightRate(235), WidthRate(120), HeightRate(20));
//    [self.registerBtn setTitle:@"注册新用户" forState:UIControlStateNormal];
//    [self.registerBtn setTitleColor:RGBA(170, 170, 170, 1) forState:UIControlStateNormal];
//    self.registerBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
//    [self.registerBtn addTarget:self action:@selector(goToRegister:) forControlEvents:UIControlEventTouchUpInside];
//    [self.contentScroll addSubview:self.registerBtn];
    
    UIButton *forgetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    forgetBtn.frame = CGRectMake(WIDTH - WidthRate(40) - WidthRate(100), self.contentScroll.frame.size.height - HeightRate(255), WidthRate(120), HeightRate(20));
    [forgetBtn setTitle:@"忘记密码" forState:UIControlStateNormal];
    [forgetBtn setTitleColor:RGBA(170, 170, 170, 1) forState:UIControlStateNormal];
    forgetBtn.titleLabel.font = font;
    [forgetBtn addTarget:self action:@selector(goToForget:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentScroll addSubview:forgetBtn];
}

- (void)buildThirdView
{
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(75), CGRectGetMaxY(self.registerBtn.frame) + HeightRate(5), WIDTH - WidthRate(150), HeightRate(2))];
    lineIV.image = [UIImage imageNamed:@"thirdLine"];
    [self.contentScroll addSubview:lineIV];
    
    CGAffineTransform matrix =  CGAffineTransformMake(1, 0, tanf(15 * (CGFloat)M_PI / 180), 1, 0, 0);
    UIFontDescriptor *desc = [ UIFontDescriptor fontDescriptorWithName :[ UIFont systemFontOfSize :17 ]. fontName matrix :matrix];
    
    UIFont *font = [UIFont fontWithDescriptor:desc size :12.0f];
    
    UILabel *thirdTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lineIV.frame) + HeightRate(5), WIDTH, HeightRate(20))];
    thirdTitleLB.textAlignment = NSTextAlignmentCenter;
    thirdTitleLB.font = font;
    thirdTitleLB.text = @"使用第三方账号直接登录";
    thirdTitleLB.textColor = RGBA(234, 91, 36, 1);
    [self.contentScroll addSubview:thirdTitleLB];
    
    for (NSInteger i = 0; i < 2; i++)
    {
        UIButton *thirdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //thirdBtn.frame = CGRectMake(WidthRate(60)  + i * (WIDTH - WidthRate(120) - WidthRate(80)) / 2, CGRectGetMaxY(thirdTitleLB.frame) + HeightRate(15), WidthRate(55), WidthRate(55));
        thirdBtn.frame = CGRectMake(WidthRate(115)  + i * (WIDTH - WidthRate(120) - WidthRate(80)) / 2, CGRectGetMaxY(thirdTitleLB.frame) + HeightRate(15), WidthRate(55), WidthRate(55));
        [thirdBtn setImage:[UIImage imageNamed:[self.thirdLoginIVNameArr objectAtIndex:i]] forState:UIControlStateNormal];
        thirdBtn.tag = 101 + i;
        thirdBtn.contentMode = UIViewContentModeScaleAspectFit;
        [thirdBtn addTarget:self action:@selector(thirdLogin:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentScroll addSubview:thirdBtn];
    }
    
//    UIButton *thirdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    thirdBtn.frame = CGRectMake(WidthRate(60)  + (WIDTH - WidthRate(120) - WidthRate(55)) / 2, CGRectGetMaxY(thirdTitleLB.frame) + HeightRate(15), WidthRate(55), WidthRate(55));
//    [thirdBtn setImage:[UIImage imageNamed:[self.thirdLoginIVNameArr objectAtIndex:1]] forState:UIControlStateNormal];
//    thirdBtn.tag = 102;
//    thirdBtn.contentMode = UIViewContentModeScaleAspectFit;
//    [thirdBtn addTarget:self action:@selector(thirdLogin:) forControlEvents:UIControlEventTouchUpInside];
//    [self.contentScroll addSubview:thirdBtn];
}


#pragma mark - 让navigationController变透明
-(void)getBackView:(UIView*)superView
{
    if ([superView isKindOfClass:NSClassFromString(@"_UINavigationBarBackground")])
    {
        for (UIView *view in superView.subviews)
        {
            if ([view isKindOfClass:[UIImageView class]])
            {
                [view removeFromSuperview];
            }
        }
        
        _navBackView = superView;
        //在这里可设置背景色
        _navBackView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
    }
    
    else if ([superView isKindOfClass:NSClassFromString(@"_UIBackdropView")])
    {
        
        //_UIBackdropEffectView是_UIBackdropView的子视图，这是只需隐藏父视图即可
        superView.hidden = YES;
    }
    
    for (UIView *view in superView.subviews)
    {
        [self getBackView:view];
    }
}

- (void)thirdLogin:(UIButton *)sender
{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    switch (sender.tag) {
        case 101:
        {
//            _tencentOAuth = [[TencentOAuth alloc] initWithAppId:qqKey andDelegate:self];
//            _tencentOAuth.redirectURI = @"www.qq.com";
//            _permissions =  [NSArray arrayWithObjects:@"get_user_info", @"get_simple_userinfo", @"add_t", nil];
//            
//            // 向服务器发出认证请求
//            [_tencentOAuth authorize:_permissions inSafari:NO];
            
            self.shadowView.hidden = NO;
        }
            break;
        case 102:
        {
            if ([WXApi isWXAppInstalled] || [WXApi isWXAppSupportApi])
            {
                [app sendAuthRequest];
//                [app sendWeixinPeople];
            }
            else
            {
                NSLog(@"还没有装微信");
            }
        }
            break;
        case 103:
        {
//            WBAuthorizeRequest *request = [WBAuthorizeRequest request];
//            request.redirectURI = @"http://www.saygogo.cn/sina/";//@"http://www.sina.com"
//            request.scope = @"all";
//            request.userInfo = @{@"SSO_From": @"ViewController",
//                                 @"Other_Info_1": [NSNumber numberWithInt:123],
//                                 @"Other_Info_2": @[@"obj1", @"obj2"],
//                                 @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
//            [WeiboSDK sendRequest:request];
            self.shadowView.hidden = NO;
            [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWrong WaringTitle:NSLocalizedString(@"微博登录暂未开放，敬请期待", nil) IsBtnShow:YES BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(alertDismiss)];
        }
            break;
        default:
            break;
    }
}

- (void)tencentDidLogin
{
    if (_tencentOAuth.accessToken && 0 != [_tencentOAuth.accessToken length])
    {
        //  记录登录用户的OpenID、Token以及过期时间
        //_labelAccessToken.text = _tencentOAuth.accessToken;
        NSLog(@"_tencentOAuth.accessToken==>%@",_tencentOAuth.accessToken);
        NSLog(@"_tencentOAuth.openid==>%@",_tencentOAuth.openId);
        NSLog(@"_tencentOAuth.time ==> %@",_tencentOAuth.expirationDate);
        
        [[NSUserDefaults standardUserDefaults] setValue:_tencentOAuth.openId forKey:@"qqOpenId"];
        [[NSUserDefaults standardUserDefaults] setValue:_tencentOAuth.accessToken forKey:@"qqToken"];
        
        NSInteger timeCuo = [_tencentOAuth.expirationDate timeIntervalSince1970] + 8 * 3600;
        [[NSUserDefaults standardUserDefaults] setValue:@(timeCuo) forKey:@"qqExpire"];
        [[NSUserDefaults standardUserDefaults] setValue:@"qqLogin" forKey:@"loginWay"];
        NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
        NSInteger time=[date timeIntervalSince1970];
        [[NSUserDefaults standardUserDefaults] setValue:@(time) forKey:@"qqLastTime"];
        
        [_tencentOAuth getUserInfo];
        
    }
    else
    {
        NSLog(@"登录不成功 没有获取accesstoken");
    }
}

- (void)getUserInfoResponse:(APIResponse*) response
{
    //NSLog(@"HHHHHHHHH ==== %@", response.jsonResponse);
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *openId = [ud objectForKey:@"qqOpenId"];
    NSString *avatar = [response.jsonResponse safeObjectForKey:@"figureurl_qq_1"];
    NSString *nickName = [response.jsonResponse safeObjectForKey:@"nickname"];
    NSString *type = @"qq";
    NSString *gender = [response.jsonResponse safeObjectForKey:@"gender"];
    NSInteger sex = 0;
    if (gender && [gender isEqualToString:@"女"])
    {
        sex = 2;
    }
    else if (gender && [gender isEqualToString:@"男"])
    {
        sex = 1;
    }
    else
    {
        sex = 0;
    }
    
    NSLog(@"json --> %@",response.jsonResponse);
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:openId forKey:@"openId"];
    [data setObject:avatar forKey:@"figureUrl"];
    [data setObject:nickName forKey:@"nickName"];
    [data setObject:@(sex) forKey:@"sex"];
    [data setObject:type forKey:@"type"];
    
    [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusThirdLogin WithParameters:data callBackBlock:^(NSDictionary *dic) {
        if ([[dic safeObjectForKey:@"result"] integerValue] == 1)
        {
            NSString *key = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"key"];
            NSString *userName = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"username"];
            
            ShoesUser *user = [ShoesUser shareUser];
            user.key = key;
            user.userName = userName;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccess" object:nil];
        }
        
    } showIndicator:NO];
    
}

- (void)thirdLoginFinish:(NSNotification *)notification
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
        if ([self.loginDelegate respondsToSelector:@selector(loginFinish)])
        {
            [self.loginDelegate loginFinish];
        }
        
    }];
}

- (void)keyBoardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView animateWithDuration:0.3f animations:^{
        
        CGRect rect = self.view.frame;
        if (IS_IOS7 && !IS_IOS8) {
            rect.origin.y = - 80;
        }else{
            rect.origin.y = - 120;
        }
        
        self.view.frame = rect;
        
    }];
}

- (void)keyBoardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:@"myAnimations" context:nil];
    CGRect rect = self.view.frame;
    rect.origin.y = 0;
    self.view.frame = rect;
    [UIView commitAnimations];
}

- (void)loginAction:(UIButton *)sender
{
    if ([self.userNameTF.text isEqualToString:@""])
    {
        [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWrong WaringTitle:@"您还没有输入账号" IsBtnShow:NO BtnTitle:@"" BtnTarget:nil BtnSEL:@selector(noWay)];
    }
    else if ([self.userPassTF.text isEqualToString:@""])
    {
        [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWrong WaringTitle:@"您还没有输入账号密码" IsBtnShow:NO BtnTitle:@"" BtnTarget:nil BtnSEL:@selector(noWay)];
    }
    else
    {
        [self.userNameTF resignFirstResponder];
        [self.userPassTF resignFirstResponder];
        
        // 加载提示
        self.shadowView.hidden = NO;
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        [self.activityIndicator setCenter:self.shadowView.center];
        [self.activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
        [self.shadowView addSubview:self.activityIndicator];
        [self.activityIndicator startAnimating];
        
        [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusLogin WithParameters:@{@"username":self.userNameTF.text,@"password":self.userPassTF.text} callBackBlock:^(NSDictionary *dic) {
            
            if ([[dic safeObjectForKey:@"result"] integerValue] == 1)
            {
                NSString *key = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"key"];
                NSString *userName = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"username"];
                NSString *memberId = [[dic safeObjectForKey:@"data"] safeObjectForKey:@"member_id"];
                
                ShoesUser *user = [ShoesUser shareUser];
                user.key = key;
                user.userName = userName;
                user.memberId = memberId;
                
                // 请求成功，隐藏加载
                self.shadowView.hidden = YES;
                [self.activityIndicator stopAnimating];
                
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                [ud setObject:self.userNameTF.text forKey:@"userAccount"];
                [ud setObject:self.userPassTF.text forKey:@"userPass"];
                [ud setObject:@"accountLogin" forKey:@"loginWay"];
                [ud synchronize];
                
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                    
                    if ([self.loginDelegate respondsToSelector:@selector(loginFinish)])
                    {
                        [self.loginDelegate loginFinish];
                    }
                    
                }];
            }
            else
            {
                NSString *message = [dic safeObjectForKey:@"message"];
                self.shadowView.hidden = NO;
                [self.activityIndicator stopAnimating];
                [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWrong WaringTitle:message IsBtnShow:YES BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(alertDismiss)];
            }
            
            
        } showIndicator:NO];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.userNameTF resignFirstResponder];
    [self.userPassTF resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.userNameTF)
    {
        if (self.userPassTF.text.length >= 5 && self.userPassTF.text.length <= 18)
        {
            if (self.userNameTF.text.length > 0)
            {
                self.loginBtn.enabled = YES;
            }
            else
            {
                self.loginBtn.enabled = NO;
            }
        }
        else
        {
            self.loginBtn.enabled = NO;
        }
    }
    else
    {
        if (self.userNameTF.text.length > 0)
        {
            if (self.userPassTF.text.length >= 5 && self.userPassTF.text.length <= 18)
            {
                self.loginBtn.enabled = YES;
            }
            else
            {
                self.loginBtn.enabled = NO;
            }
        }
        else
        {
            self.loginBtn.enabled = NO;
        }
    }
    
    return YES;
}

- (void)alertDismiss
{
    [UIView animateWithDuration:0.3f animations:^{
        
        [[ShoesAlertView shareAlertView] hideTip:self.view];
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

- (void)noWay
{
    
}

- (void)goToRegister:(UIButton *)sender
{
    ShoesMyPhoneVercodeViewController *phoneCodeVC = [[ShoesMyPhoneVercodeViewController alloc] init];
    phoneCodeVC.isForgotPass = NO;
    [self.navigationController pushViewController:phoneCodeVC animated:YES];
}

- (void)regisnAllResponse:(UITapGestureRecognizer *)tap
{
    [self.userPassTF resignFirstResponder];
    [self.userNameTF resignFirstResponder];
}

- (void)goToForget:(UIButton *)sender
{
    ShoesMyPhoneVercodeViewController *phoneCodeVC = [[ShoesMyPhoneVercodeViewController alloc] init];
    phoneCodeVC.isForgotPass = YES;
    [self.navigationController pushViewController:phoneCodeVC animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.userNameTF resignFirstResponder];
    [self.userPassTF resignFirstResponder];
}

- (void)loginCancel:(UIButton *)sender
{
    if ([self.loginDelegate respondsToSelector:@selector(loginHasCancel)])
    {
        [self.loginDelegate loginHasCancel];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
    
}

@end
