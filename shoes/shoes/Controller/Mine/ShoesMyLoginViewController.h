//
//  ShoesMyLoginViewController.h
//  shoes
//
//  Created by ZhangEapin on 15/9/20.
//  Copyright © 2015年 saygogo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShoesMyLoginViewControllerDelegate <NSObject>

- (void)loginFinish;
- (void)loginHasCancel;

@end

@interface ShoesMyLoginViewController : UIViewController

@property (weak,nonatomic) id<ShoesMyLoginViewControllerDelegate> loginDelegate;
@property (assign,nonatomic) BOOL isFromLogout;

@end
