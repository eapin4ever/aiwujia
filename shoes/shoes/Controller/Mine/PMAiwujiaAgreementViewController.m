//
//  PMAiwujiaAgreementViewController.m
//  shoes
//
//  Created by P&M on 16/1/4.
//  Copyright © 2016年 saygogo. All rights reserved.
//

#import "PMAiwujiaAgreementViewController.h"

@interface PMAiwujiaAgreementViewController () <UIWebViewDelegate>

@property (strong, nonatomic) UIView *backgroundView;
@property (strong, nonatomic) UIWebView *noticeWebView;

@end

@implementation PMAiwujiaAgreementViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
    self.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"loginBackground"]];
    [self.view addSubview:self.backgroundView];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn setImage:[UIImage imageNamed:@"shoesBack"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backToView:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    
    _noticeWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    _noticeWebView.backgroundColor = [UIColor clearColor];
    [_noticeWebView setUserInteractionEnabled:YES];  //是否支持交互
    [_noticeWebView setDelegate:self];
    _noticeWebView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    //    [self.view addSubview:_noticeWebView];
    [self.backgroundView insertSubview:_noticeWebView atIndex:0];
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", agreementIP]];//创建URL
    
    [_noticeWebView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)backToView:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
