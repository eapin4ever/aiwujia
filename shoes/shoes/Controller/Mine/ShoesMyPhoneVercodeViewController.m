//
//  ShoesMyPhoneVercodeViewController.m
//  shoes
//
//  Created by ZhangEapin on 15/9/20.
//  Copyright © 2015年 saygogo. All rights reserved.
//

#import "ShoesMyPhoneVercodeViewController.h"
#import "CustomerTextField.h"
#import "ShoesSettingPassViewController.h"
#import "ShoesCheckUtil.h"

@interface ShoesMyPhoneVercodeViewController () <UITextFieldDelegate>

@property (strong,nonatomic) UIView *backgroundView;
@property (strong,nonatomic) UIImageView *backgroundIma;
@property (strong,nonatomic) UIView *logoView;
@property (strong,nonatomic) UIView *tipView;
@property (strong,nonatomic) CustomerTextField *phoneNumTF;
@property (strong,nonatomic) CustomerTextField *verCodeTF;
@property (strong,nonatomic) UIView *inputView;
@property (strong,nonatomic) UIButton *getCodeBtn;
@property (strong,nonatomic) UILabel *sendTipLB;
@property (strong,nonatomic) UIButton *nextBtn;
@property (strong,nonatomic) UIView *shadowView;
@property (assign,nonatomic) NSInteger timeCount;
@property (strong,nonatomic) NSTimer *timer;

@end

@implementation ShoesMyPhoneVercodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [self.view addSubview:self.backgroundView];
    
    self.backgroundIma = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.backgroundView.frame.size.width, self.backgroundView.frame.size.height)];
    self.backgroundIma.image = [UIImage imageNamed:@"loginBackground"];
    self.backgroundIma.userInteractionEnabled = YES;
    [self.backgroundView addSubview:self.backgroundIma];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.backgroundView addGestureRecognizer:swipe];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn setImage:[UIImage imageNamed:@"shoesBack"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.timeCount = 0;
    
    [self buildLogoView];
    //[self buildTipView];
    [self buildInputView];
    [self buildNextBtnView];
    [self buildShadowView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyBoard:)];
    [self.backgroundView addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildLogoView
{
    self.logoView = [[UIView alloc] initWithFrame:CGRectMake(0, 70, WIDTH, HeightRate(150))];
    self.logoView.backgroundColor = [UIColor clearColor];
    [self.backgroundIma addSubview:self.logoView];
    
    UIImageView *logoIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"shareIcon"]];
    logoIV.contentMode = UIViewContentModeScaleAspectFit;
    logoIV.center = CGPointMake(WIDTH / 2, HeightRate(40));
    logoIV.bounds = (CGRect){CGPointZero,{HeightRate(95),HeightRate(95)}};
    [self.logoView addSubview:logoIV];
    
    UIImageView *logoTitleIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logoTitle"]];
    logoTitleIV.contentMode = UIViewContentModeScaleAspectFit;
    logoTitleIV.center = CGPointMake(WIDTH / 2, HeightRate(105));
    logoTitleIV.bounds = (CGRect){CGPointZero,{WIDTH,HeightRate(30)}};
    [self.logoView addSubview:logoTitleIV];
}

- (void)buildTipView
{
//    CGAffineTransform matrix =  CGAffineTransformMake(1, 0, tanf(15 * (CGFloat)M_PI / 180), 1, 0, 0);
//    UIFontDescriptor *desc = [ UIFontDescriptor fontDescriptorWithName :[ UIFont systemFontOfSize :17 ]. fontName matrix :matrix];
    
//    UIFont *font = [UIFont fontWithDescriptor:desc size :16.0f];
    UIFont *font = [UIFont systemFontOfSize:16.0f];
    
    self.tipView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.logoView.frame) + HeightRate(30), WIDTH, HeightRate(20))];
    self.tipView.backgroundColor = [UIColor whiteColor];
    UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tipView.bounds.size.width, self.tipView.bounds.size.height)];
    tipLB.font = font;
    tipLB.textAlignment = NSTextAlignmentCenter;
    tipLB.textColor = RGBA(243, 91, 36, 1);
    tipLB.text = @"用户注册Step_安全验证";
    [self.tipView addSubview:tipLB];
    [self.backgroundIma addSubview:self.tipView];
    
}

- (void)buildInputView
{
    self.inputView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.logoView.frame) + HeightRate(30), WIDTH, HeightRate(145))];
    self.inputView.backgroundColor = [UIColor clearColor];
    [self.backgroundIma addSubview:self.inputView];
    
//    CGAffineTransform matrix =  CGAffineTransformMake(1, 0, tanf(15 * (CGFloat)M_PI / 180), 1, 0, 0);
//    UIFontDescriptor *desc = [ UIFontDescriptor fontDescriptorWithName :[ UIFont systemFontOfSize :17 ]. fontName matrix :matrix];
    
//    UIFont *font = [UIFont fontWithDescriptor:desc size :14.0f];
    
    UIView *mobileView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(50), 0, WIDTH - WidthRate(100), HeightRate(65))];
    mobileView.backgroundColor = [UIColor clearColor];
    [self.inputView addSubview:mobileView];
    
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(0, mobileView.frame.size.height - 1, mobileView.frame.size.width, 1);
    layer1.backgroundColor = RGBA(222, 222, 222, 1).CGColor;
    [mobileView.layer addSublayer:layer1];
    
    UIImageView *nameIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(5), HeightRate(15), WidthRate(25), HeightRate(35))];
    nameIV.image = [UIImage imageNamed:@"mobileIma"];
    nameIV.contentMode = UIViewContentModeScaleAspectFit;
    nameIV.userInteractionEnabled = YES;
    [mobileView addSubview:nameIV];
    
    
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    
    self.phoneNumTF = [[CustomerTextField alloc] initWithFrame:CGRectMake(WidthRate(50), HeightRate(10), mobileView.bounds.size.width - WidthRate(50), HeightRate(50))];
    self.phoneNumTF.placeholder = @"手机号/用户名";
    [self.phoneNumTF setValue:RGBA(145, 145, 145, 1) forKeyPath:@"_placeholderLabel.textColor"];
    [self.phoneNumTF setValue:font forKeyPath:@"_placeholderLabel.font"];
    self.phoneNumTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.phoneNumTF.delegate = self;
    self.phoneNumTF.returnKeyType = UIReturnKeyDone;
    [mobileView addSubview:self.phoneNumTF];
    
    
    UIView *verCodeView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(50), CGRectGetMaxY(mobileView.frame), WIDTH - WidthRate(100), HeightRate(65))];
    verCodeView.backgroundColor = [UIColor clearColor];
    [self.inputView addSubview:verCodeView];
    
    CALayer *layer2 = [CALayer layer];
    layer2.frame = CGRectMake(0, verCodeView.frame.size.height - 1, verCodeView.frame.size.width, 1);
    layer2.backgroundColor = RGBA(222, 222, 222, 1).CGColor;
    [verCodeView.layer addSublayer:layer2];
    
    UIImageView *verCodeViewIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(5), HeightRate(15), WidthRate(25), HeightRate(35))];
    verCodeViewIV.image = [UIImage imageNamed:@"vercodeIma"];
    verCodeViewIV.contentMode = UIViewContentModeScaleAspectFit;
    verCodeViewIV.userInteractionEnabled = YES;
    [verCodeView addSubview:verCodeViewIV];
    
    self.verCodeTF = [[CustomerTextField alloc] initWithFrame:CGRectMake(WidthRate(50), HeightRate(10), WidthRate(120), HeightRate(50))];
    self.verCodeTF.placeholder = @"请输入验证码";
    [self.verCodeTF setValue:RGBA(145, 145, 145, 1) forKeyPath:@"_placeholderLabel.textColor"];
    [self.verCodeTF setValue:font forKeyPath:@"_placeholderLabel.font"];
    self.verCodeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.verCodeTF.delegate = self;
    self.verCodeTF.tag = 2;
    self.verCodeTF.keyboardType = UIKeyboardTypeNumberPad;
    [verCodeView addSubview:self.verCodeTF];
    
    self.getCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.getCodeBtn.frame = CGRectMake(CGRectGetMaxX(self.verCodeTF.frame) + WidthRate(10), verCodeViewIV.frame.origin.y, (WIDTH - WidthRate(120)) * 0.4 - 10, HeightRate(35));
    [self.getCodeBtn setTitle:@"获取安全码" forState:UIControlStateNormal];
    [self.getCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.getCodeBtn addTarget:self action:@selector(getVerCode:) forControlEvents:UIControlEventTouchUpInside];
    self.getCodeBtn.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    [self.getCodeBtn.layer setCornerRadius:3.0f];
    [self.getCodeBtn.layer setBorderWidth:1.0f];
    [self.getCodeBtn.layer setBorderColor:RGBA(168, 75, 149, 1).CGColor];
    [self.getCodeBtn setBackgroundColor:RGBA(175, 84, 156, 1)];
    [verCodeView addSubview:self.getCodeBtn];
    
//    UIFont *font2 = [UIFont fontWithDescriptor:desc size :12.0f];
    UIFont *font2 = [UIFont systemFontOfSize:WidthRate(13)];
    self.sendTipLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(50), CGRectGetMaxY(verCodeView.frame) + HeightRate(5), WIDTH - WidthRate(100), HeightRate(20))];
    self.sendTipLB.textAlignment = NSTextAlignmentRight;
    self.sendTipLB.textColor = RGBA(205, 205, 205, 1);
    self.sendTipLB.font = font2;
    self.sendTipLB.text = @"安全码已发送至您的手机,请查收！";
    //self.sendTipLB.hidden = YES;
    [self.inputView addSubview:self.sendTipLB];
    
}

- (void)buildNextBtnView
{
    self.nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextBtn.frame = CGRectMake(WidthRate(50), CGRectGetMaxY(self.inputView.frame) + HeightRate(30), WIDTH - WidthRate(100), HeightRate(44));
    [self.nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
    self.nextBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [self.nextBtn addTarget:self action:@selector(goToSettingPass:) forControlEvents:UIControlEventTouchUpInside];
    [self.nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.nextBtn.layer setCornerRadius:3.0f];
    [self.nextBtn.layer setBorderWidth:1.0f];
    [self.nextBtn.layer setBorderColor:RGBA(226, 38, 57, 1).CGColor];
    [self.nextBtn setBackgroundColor:RGBA(228, 48, 90, 1)];
    [self.backgroundIma addSubview:self.nextBtn];
}

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)keyBoardWillShow:(NSNotification *)notification
{
    if (self.verCodeTF.tag == 2) {
        
        NSDictionary *userInfo = [notification userInfo];
        
        NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
        NSTimeInterval animationDuration;
        [animationDurationValue getValue:&animationDuration];
        
        [UIView animateWithDuration:0.3f animations:^{
            
            CGRect rect = self.view.frame;
            if (IS_IOS7 && !IS_IOS8) {
                rect.origin.y = - 80;
            }else{
                rect.origin.y = - 120;
            }
            
            self.view.frame = rect;
            
        }];
    }
}

- (void)keyBoardWillHide:(NSNotification *)notification
{
    if (self.verCodeTF.tag == 2) {
        
        [UIView beginAnimations:@"myAnimations" context:nil];
        CGRect rect = self.view.frame;
        rect.origin.y = 0;
        self.view.frame = rect;
        [UIView commitAnimations];
    }
}

- (void)goToSettingPass:(UIButton *)sender
{
    if ([self.phoneNumTF.text isEqualToString:@""])
    {
        self.shadowView.hidden = NO;
        [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWrong WaringTitle:@"请输入手机号码" IsBtnShow:YES BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(noWay)];
    }
    else if ([self.verCodeTF.text isEqualToString:@""])
    {
        self.shadowView.hidden = NO;
        [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWrong WaringTitle:@"请输入验证码" IsBtnShow:YES BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(noWay)];
    }
    else
    {
        [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusCheckSMS WithParameters:@{@"verifyCode":self.verCodeTF.text,@"mobile":self.phoneNumTF.text} callBackBlock:^(NSDictionary *dic) {
            if ([[dic safeObjectForKey:@"result"] integerValue] == 1)
            {
                ShoesSettingPassViewController *passSettingVC = [[ShoesSettingPassViewController alloc] init];
                passSettingVC.isForget = self.isForgotPass;
                if (self.isForgotPass)
                {
                    passSettingVC.verCode = self.verCodeTF.text;
                }
                passSettingVC.phoneString = self.phoneNumTF.text;
                [self.navigationController pushViewController:passSettingVC animated:YES];
                
            }
            else
            {
                self.shadowView.hidden = NO;
                NSString *message = [dic safeObjectForKey:@"message"];
                [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWrong WaringTitle:message IsBtnShow:YES BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(noWay)];
            }
            
        } showIndicator:NO];
    }
}

- (void)noWay
{
    [UIView animateWithDuration:0.3f animations:^{
        
        [[ShoesAlertView shareAlertView] hideTip:self.view];
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

- (void)buildShadowView
{
    self.shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.shadowView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.shadowView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.shadowView.bounds.size.width, self.shadowView.bounds.size.height)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.6;
    [self.shadowView addSubview:alphaView];
    
    self.shadowView.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[ShoesAlertView shareAlertView] hideTip:self.view];
    self.shadowView.hidden = YES;
}

- (void)getVerCode:(UIButton *)sender
{
    [self.phoneNumTF resignFirstResponder];
    [self.verCodeTF resignFirstResponder];
    
    if ([self.phoneNumTF.text isEqualToString:@""])
    {
        self.shadowView.hidden = NO;
        [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWrong WaringTitle:@"您还没有输入手机号码" IsBtnShow:YES BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(noWay)];
    }
    else if (![ShoesCheckUtil validateMobile:self.phoneNumTF.text])
    {
        self.shadowView.hidden = NO;
        [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWrong WaringTitle:@"请输入正确的手机号码" IsBtnShow:YES BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(noWay)];
    }
    else
    {
        [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusGetSMS WithParameters:@{@"mobile":self.phoneNumTF.text,@"type":self.isForgotPass ? @(0) : @(1)} callBackBlock:^(NSDictionary *dic) {
            if ([[dic safeObjectForKey:@"result"] integerValue] == 1)
            {
                self.sendTipLB.hidden = NO;
                self.timeCount = 60;
                self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(changeTimerNum) userInfo:nil repeats:YES];
                [self.timer fire];
            }
            else
            {
                NSString *message = [dic safeObjectForKey:@"message"];
                self.shadowView.hidden = NO;
                [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWrong WaringTitle:message IsBtnShow:YES BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(noWay)];
            }
            
            
        } showIndicator:NO];
        
        
    }
}

- (void)changeTimerNum
{
    if (self.timeCount > 0) {
        self.getCodeBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [self.getCodeBtn setTitle:[NSString stringWithFormat:@"%@ 秒",@(self.timeCount)] forState:UIControlStateDisabled];
        [self.getCodeBtn setEnabled:NO];
        self.timeCount--;
    }else {
        self.getCodeBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.getCodeBtn setTitle:@"重新获取验证码" forState: UIControlStateNormal];
        [self.getCodeBtn setEnabled:YES];
        [self.timer invalidate];
    }
}

- (void)resignKeyBoard:(UITapGestureRecognizer *)tap
{
    [self.phoneNumTF resignFirstResponder];
    [self.verCodeTF resignFirstResponder];
}

@end
