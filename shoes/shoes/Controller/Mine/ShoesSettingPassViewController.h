//
//  ShoesSettingPassViewController.h
//  shoes
//
//  Created by pmit on 15/9/21.
//  Copyright © 2015年 saygogo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoesSettingPassViewController : UIViewController

@property (copy,nonatomic) NSString *phoneString;
@property (assign,nonatomic) BOOL isForget;
@property (copy,nonatomic) NSString *verCode;


@end
