//
//  ZSTWebFailureViewController.m
//  shoes
//
//  Created by pmit on 16/1/12.
//  Copyright © 2016年 saygogo. All rights reserved.
//

#import "ZSTWebFailureViewController.h"
#import "AppDelegate.h"

@interface ZSTWebFailureViewController ()

@property (strong,nonatomic) UIView *navBackView;

@end

@implementation ZSTWebFailureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self buildImageView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getBackView:self.navigationController.navigationBar];
    [UIApplication sharedApplication].statusBarHidden = YES;
}

#pragma mark - 让navigationController变透明
-(void)getBackView:(UIView*)superView
{
    if ([superView isKindOfClass:NSClassFromString(@"_UINavigationBarBackground")])
    {
        for (UIView *view in superView.subviews)
        {
            if ([view isKindOfClass:[UIImageView class]])
            {
                [view removeFromSuperview];
            }
        }
        
        _navBackView = superView;
        //在这里可设置背景色
//        _navBackView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        _navBackView.backgroundColor = [UIColor whiteColor];
    }
    
    else if ([superView isKindOfClass:NSClassFromString(@"_UIBackdropView")])
    {
        
        //_UIBackdropEffectView是_UIBackdropView的子视图，这是只需隐藏父视图即可 
        superView.hidden = YES;
    }
    
    for (UIView *view in superView.subviews)
    {
        [self getBackView:view];
    }
}

- (void)buildImageView
{
    UIImageView *errorIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, -64, WIDTH, HEIGHT + 64)];
    errorIV.image = [UIImage imageNamed:@"webError"];
    errorIV.userInteractionEnabled = YES;
    [self.view addSubview:errorIV];
    
    UIButton *exitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    exitBtn.frame = CGRectMake((WIDTH - WidthRate(220)) / 2, HEIGHT + 64 - HeightRate(160), WidthRate(220), WidthRate(220) * 106 / 353);
    [exitBtn setImage:[UIImage imageNamed:@"exitBtn"] forState:UIControlStateNormal];
    [exitBtn addTarget:self action:@selector(exitNow:) forControlEvents:UIControlEventTouchUpInside];
    [errorIV addSubview:exitBtn];
    
}

- (void)exitNow:(UIButton *)sender
{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [UIView animateWithDuration:1.0f animations:^{
        app.window.alpha = 0;
//        app.window.frame = CGRectMake(0, 0, 0, 0);
    } completion:^(BOOL finished) {
        exit(0);
    }];
}


@end
