//
//  ShoesWebViewController.m
//  shoes
//
//  Created by pmit on 15/9/9.
//  Copyright (c) 2015年 saygogo. All rights reserved.
//

#import "ShoesWebViewController.h"
#import "ShoesUser.h"
#import "ZSTNewShareView.h"
#import <MessageUI/MessageUI.h>
#import "ShoesMyLoginViewController.h"
#import "ShoesTabbarBtn.h"
#import "AppDelegate.h"
#import "ZSTWebFailureViewController.h"
#import "NJKWebViewProgressView.h"
#import "NJKWebViewProgress.h"

@interface ShoesWebViewController () <UIWebViewDelegate,ShoesMyLoginViewControllerDelegate,ZSTNewShareViewDelegate,MFMessageComposeViewControllerDelegate,UIScrollViewDelegate,NJKWebViewProgressDelegate>

@property (strong,nonatomic) UIWebView *shoesWebView;
//@property (strong,nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong,nonatomic) UIView *shadowView;
@property (strong,nonatomic) UIButton *backBtn;
@property (strong,nonatomic) UIView *funView;
@property (assign,nonatomic) BOOL isFunOpen;
@property (strong,nonatomic) ZSTNewShareView *shareView;
@property (strong,nonatomic) UIView *shareShadowView;
@property (copy,nonatomic) NSString *shareUrl;
@property (strong,nonatomic) NSArray *tabberItemArr;
@property (strong,nonatomic) NSArray *tabbarImteImgArr;
@property (strong,nonatomic) NSArray *selectImteImgArr;
@property (strong,nonatomic) UIView *tabbarView;
@property (strong,nonatomic) ShoesTabbarBtn *selectBtn;
@property (copy,nonatomic) NSString *shareTitle;
@property (strong,nonatomic) UIView *darkView;
@property (copy,nonatomic) NSString *updateUrlString;
@property (strong,nonatomic) UIView *remindDarkView;
@property (strong,nonatomic) UIView *alertView;
@property (copy,nonatomic) NSString *loginTypeString;


@property (strong, nonatomic) NSString *secretAgent;
@property (assign, nonatomic) BOOL isUpdataFunView;
@property (assign, nonatomic) BOOL isGoodsList;
@property (strong,nonatomic)  NJKWebViewProgressView *progressView;
@property (strong,nonatomic) NJKWebViewProgress *progressProxy;

@end

@implementation ShoesWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.tabberItemArr = @[@"首页",@"购物车",@"商品搜索",@"个人中心",@"购买咨询"];
    self.tabberItemArr = @[@"首页",@"购物车",@"商品搜索",@"个人中心"];
//    self.tabbarImteImgArr = @[@"home",@"car",@"classify",@"individual",@"consult"];
    self.tabbarImteImgArr = @[@"home",@"car",@"classify",@"individual"];
//    self.selectImteImgArr = @[@"homeHighLight",@"carHighLight",@"classifyHighLight",@"individualHighLight"];
    self.selectImteImgArr = @[@"homeHighLight",@"carHighLight",@"classifyHighLight",@"individualHighLight",@"consultHighLight"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDidSuccess:) name:@"loginSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(weixinPayCallBack:) name:@"weixinPaySuccess" object:nil];
    
    self.isFunOpen = NO;
    self.isUpdataFunView = NO;
    self.isGoodsList = NO;
    
    self.navigationController.navigationBar.barTintColor = RGBA(228, 48, 90, 1);
    
    
    
    [self buildHeader];
    [self buildWebView];
    [self buildTabbarView];
//    [self buildWaitingView];
    [self buildFunView];
    [self buildShareShadowVied];
    [self createShareView];
    
//    [self checkUpdate];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [UIApplication sharedApplication].statusBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//    if (![ShoesUser shareUser].key || [[ShoesUser shareUser].key isEqualToString:@""])
//    {
//        ShoesMyLoginViewController *loginVC = [[ShoesMyLoginViewController alloc] init];
//        loginVC.loginDelegate = self;
//        loginVC.isFromLogout = NO;
//        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:loginVC];
//        [self presentViewController:navi animated:YES completion:nil];
//    }
//    else
//    {
//        
//    }
    [self.navigationController.navigationBar addSubview:_progressView];
    [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webUrl]]];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_progressView removeFromSuperview];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildHeader
{
    //设置返回按钮
    self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.backBtn.frame = CGRectMake(0, 0, 15, 15);
    [self.backBtn setImage:[UIImage imageNamed:@"turnBack"] forState:UIControlStateNormal];
    [self.backBtn addTarget:self action:@selector(goToPre:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.backBtn];
    self.backBtn.hidden = YES;
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WidthRate(100), HeightRate(30))];
    titleView.backgroundColor = [UIColor clearColor];
    self.navigationItem.titleView = titleView;
    
    UIImageView *titleIma = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleView.frame.size.width, titleView.frame.size.height)];
    titleIma.image = [UIImage imageNamed:@"titleIma"];
    titleIma.contentMode = UIViewContentModeScaleAspectFit;
    [titleView addSubview:titleIma];
    
    
    UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoutBtn.frame = CGRectMake(0, 0, 40, 20);
    [logoutBtn setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
    [logoutBtn addTarget:self action:@selector(showOtherFun:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:logoutBtn];

}

- (void)buildTabbarView
{
    CGFloat tabbarHeight = iPhone4s ? HeightRate(58) : HeightRate(55);
    
    UIView *tabbarView = [[UIView alloc] initWithFrame:CGRectMake(0 , HEIGHT - tabbarHeight, WIDTH , tabbarHeight)];
    tabbarView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:tabbarView];
    
    self.tabbarView = tabbarView;
    
    for (NSInteger i = 0; i < self.tabbarImteImgArr.count; i++)
    {
        ShoesTabbarBtn *tabbarBtn = [ShoesTabbarBtn buttonWithType:UIButtonTypeCustom];
        tabbarBtn.frame = CGRectMake(i * WIDTH / 4, 0, WIDTH / 4, tabbarHeight);
        tabbarBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        tabbarBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        tabbarBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [tabbarBtn setTitle:self.tabberItemArr[i] forState:UIControlStateNormal];
        [tabbarBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [tabbarBtn setTitleColor:RGBA(228, 48, 90, 1) forState:UIControlStateSelected];
        [tabbarBtn setImage:[UIImage imageNamed:self.tabbarImteImgArr[i]] forState:UIControlStateNormal];
        [tabbarBtn setImage:[UIImage imageNamed:self.selectImteImgArr[i]] forState:UIControlStateSelected];
        tabbarBtn.tag = i + 1;
        [tabbarBtn addTarget:self action:@selector(jumpToSomeWeb:) forControlEvents:UIControlEventTouchUpInside];
        [tabbarView addSubview:tabbarBtn];
        
        if (i == 0)
        {
            self.selectBtn = tabbarBtn;
            tabbarBtn.selected = YES;
        }
    }
}



- (void)buildWebView
{
    NSString *oldAgent = [[[UIWebView alloc] init] stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    NSLog(@"old agent :%@", oldAgent);
    
    //add my info to the new agent
    NSString *newAgent = [oldAgent stringByAppendingString:@";PmitClient"];
    NSLog(@"new agent :%@", newAgent);
    
    //regist the new agent
    NSDictionary *dictionnary = [[NSDictionary alloc] initWithObjectsAndKeys:newAgent, @"UserAgent", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionnary];
    
    self.shoesWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - HeightRate(55))];
    self.shoesWebView.scrollView.delegate = self;
    self.shoesWebView.scrollView.decelerationRate = 1;
//    self.shoesWebView.delegate = self;
    
    _progressProxy = [[NJKWebViewProgress alloc] init];
    self.shoesWebView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigationBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigationBarBounds.size.height, navigationBarBounds.size.width, progressBarHeight);
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    [self.view addSubview:self.shoesWebView];
}

- (void)loginFinish
{
    self.selectBtn.selected = NO;
    if ([self.loginTypeString isEqualToString:@"shareLogin"])
    {
        [self.alertView removeFromSuperview];
        self.alertView = nil;
        [self.remindDarkView removeFromSuperview];
        self.remindDarkView = nil;
        
        if (self.isGoodsList) {
            
            self.selectBtn.selected = NO;
            ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:3];
            firstBtn.selected = YES;
            self.selectBtn = firstBtn;
            
            [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.fyaiwujia.com/mobile/index.php?act=goods&op=m_goods_list&id=7"]]];
        }
        else {
            self.selectBtn.selected = NO;
            ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:1];
            firstBtn.selected = YES;
            self.selectBtn = firstBtn;
        }
        
        [self showShareView];
    }
    else if ([self.loginTypeString isEqualToString:@"carlogin"])
    {
        [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.fyaiwujia.com/mobile/index.php?act=member_cart&op=m_cart_list"]]];
        ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:2];
        firstBtn.selected = YES;
        self.selectBtn = firstBtn;
    }
    else if ([self.loginTypeString isEqualToString:@"personallogin"])
    {
        [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.fyaiwujia.com/mobile/index.php?act=member_index&op=m_personal_center"]]];
        ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:4];
        firstBtn.selected = YES;
        self.selectBtn = firstBtn;
    }
    else if ([self.loginTypeString isEqualToString:@"asklogin"])
    {
        [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.fyaiwujia.com/mobile/index.php?act=goods&op=askquestion&id=7"]]];
        ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:5];
        firstBtn.selected = YES;
        self.selectBtn = firstBtn;
    }
    else
    {
        [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webUrl]]];
        ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:1];
        firstBtn.selected = YES;
        self.selectBtn = firstBtn;
        
        [self buildFunView];
    }
    
    self.funView.frame = CGRectMake(self.funView.frame.origin.x, self.funView.frame.origin.y, self.funView.bounds.size.width, HeightRate(161.5));
    
    UIButton *logoutBtn = (UIButton *)[self.funView viewWithTag:1003];
    if (!logoutBtn)
    {
        [self buildFunLB:CGRectMake(0, HeightRate(121.5), WidthRate(150), HeightRate(40)) Title:@"注销" BtnTag:1003 FatherView:self.funView];
    }
    
}

//- (void)buildWaitingView
//{
//    self.shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
//    self.shadowView.backgroundColor = [UIColor clearColor];
//    [self.view addSubview:self.shadowView];
//    
//    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.shadowView.bounds.size.width, self.shadowView.bounds.size.height)];
//    alphaView.backgroundColor = [UIColor blackColor];
//    alphaView.alpha = 0.6;
//    [self.shadowView addSubview:alphaView];
//    
//    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0,0,50,50)];
//    [self.activityIndicator setCenter:self.shadowView.center];
//    [self.activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
//    [self.shadowView addSubview:self.activityIndicator];
//    
//    self.shadowView.hidden = YES;
//}

- (void)goToPre:(UIButton *)sender
{
    [self.shoesWebView goBack];
}

- (void)logout:(UIButton *)sender
{
    [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusLogout WithParameters:@{@"key":[ShoesUser shareUser].key,@"username":[ShoesUser shareUser].userName} callBackBlock:^(NSDictionary *dic) {
        if ([[dic safeObjectForKey:@"result"] integerValue] == 1)
        {
            self.loginTypeString = @"";
            [ShoesUser shareUser].key = @"";
            [ShoesUser shareUser].userName = @"";
            [ShoesUser shareUser].memberId = @"";
            
            // 退出默认选中首页
            self.selectBtn.selected = NO;
            ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:1];
            firstBtn.selected = YES;
            self.selectBtn = firstBtn;
            
            ShoesMyLoginViewController *loginVC = [[ShoesMyLoginViewController alloc] init];
            loginVC.isFromLogout = YES;
            loginVC.loginDelegate = self;
            UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:loginVC];
            [self presentViewController:navi animated:YES completion:nil];
        }
        
    } showIndicator:NO];
}

- (void)buildFunView
{
    self.funView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(150), - HeightRate(202), WidthRate(150), HeightRate(202))];
    self.funView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.funView];
    
    CALayer *firstLine = [CALayer layer];
    firstLine.frame = CGRectMake(0, HeightRate(40), WidthRate(150), 0.5);
    firstLine.backgroundColor = [UIColor darkGrayColor].CGColor;
    [self.funView.layer addSublayer:firstLine];
    
    CALayer *secondLine = [CALayer layer];
    secondLine.frame = CGRectMake(0, HeightRate(80.5), WidthRate(150), 0.5);
    secondLine.backgroundColor = [UIColor darkGrayColor].CGColor;
    [self.funView.layer addSublayer:secondLine];
    
    CALayer *thirdLine = [CALayer layer];
    thirdLine.frame = CGRectMake(0, HeightRate(121), WidthRate(150), 0.5);
    thirdLine.backgroundColor = [UIColor darkGrayColor].CGColor;
    [self.funView.layer addSublayer:thirdLine];
    
    CALayer *forthLine = [CALayer layer];
    forthLine.frame = CGRectMake(0, HeightRate(161.5), WidthRate(150), 0.5);
    forthLine.backgroundColor = [UIColor darkGrayColor].CGColor;
    [self.funView.layer addSublayer:forthLine];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_build = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    [self buildFunLB:CGRectMake(0, 0, WidthRate(150), HeightRate(40)) Title:[NSString stringWithFormat:@"版本:%@",app_build] BtnTag:9 FatherView:self.funView];
    [self buildFunLB:CGRectMake(0, HeightRate(40.5), WidthRate(150), HeightRate(40)) Title:@"前进" BtnTag:1000 FatherView:self.funView];
    [self buildFunLB:CGRectMake(0, HeightRate(81), WidthRate(150), HeightRate(40)) Title:@"刷新" BtnTag:1001 FatherView:self.funView];
    [self buildFunLB:CGRectMake(0, HeightRate(121.5), WidthRate(150), HeightRate(40)) Title:@"分享" BtnTag:1002 FatherView:self.funView];
    if (![ShoesUser shareUser].key || [[ShoesUser shareUser].key isEqualToString:@""])
    {
        self.funView.frame = CGRectMake(self.funView.frame.origin.x, self.funView.frame.origin.y, self.funView.bounds.size.width, HeightRate(161.5));
    }
    else
    {
        [self buildFunLB:CGRectMake(0, HeightRate(162), WidthRate(150), HeightRate(40)) Title:@"注销" BtnTag:1003 FatherView:self.funView];
        self.funView.frame = CGRectMake(self.funView.frame.origin.x, self.funView.frame.origin.y, self.funView.bounds.size.width, HeightRate(202));
    }
    
}

- (void)showOtherFun:(UIButton *)sender
{
    if (self.isFunOpen)
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.funView.frame = CGRectMake(WIDTH - WidthRate(150), - self.funView.bounds.size.height, WidthRate(150), self.funView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            self.isFunOpen = NO;
        }];
    
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.funView.frame = CGRectMake(WIDTH - WidthRate(150), 64, WidthRate(150), self.funView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            self.isFunOpen = YES;
            
        }];

    }
}

#pragma mark - 工具Label重构
- (void)buildFunLB:(CGRect)rect Title:(NSString *)title BtnTag:(NSInteger)btnTag FatherView:(UIView *)fatherView
{
    UIView *alphaView = [[UIView alloc] initWithFrame:rect];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.7;
    [fatherView addSubview:alphaView];
    
    UILabel *funLB = [[UILabel alloc] initWithFrame:CGRectMake(5, rect.origin.y, rect.size.width - 10, rect.size.height)];
    funLB.textColor = [UIColor whiteColor];
    funLB.text = title;
    funLB.font = [UIFont systemFontOfSize:16.0f];
    funLB.textAlignment = NSTextAlignmentLeft;
    [fatherView addSubview:funLB];
    
    UIButton *clickBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    clickBtn.frame = rect;
    clickBtn.tag = btnTag;
    [clickBtn addTarget:self action:@selector(funAction:) forControlEvents:UIControlEventTouchUpInside];
    [fatherView addSubview:clickBtn];
}

- (void)funAction:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 1000:
            
            if ([self.shoesWebView canGoForward])
            {
                [self.shoesWebView goForward];
            }
            else
            {
                self.shadowView.hidden = NO;
                [[ShoesAlertView shareAlertView] showInView:self.view WaringType:ShoesWarningTypeWarning WaringTitle:@"亲，已经到最后了" IsBtnShow:YES BtnTitle:@"我知道了" BtnTarget:self BtnSEL:@selector(alertDismiss)];
            }
            
            break;
        case 1001:
            
            [self.shoesWebView reload];
            
            break;
        case 1002:
            
            if (![ShoesUser shareUser].key || [[ShoesUser shareUser].key isEqualToString:@""])
            {
                [self buildRemindView];
            }
            else
            {
                [self showShareView];
            }
            
            
            break;
        case 1003:
            
            [self logout:nil];
            
            break;
            
        default:
            break;
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        
        self.funView.frame = CGRectMake(WIDTH - WidthRate(150), - HeightRate(161.5), WidthRate(150), HeightRate(161.5));
        
    } completion:^(BOOL finished) {
        
        self.isFunOpen = NO;
    }];
}

- (void)alertDismiss
{
    [UIView animateWithDuration:0.3f animations:^{
        
        [[ShoesAlertView shareAlertView] hideTip:self.view];
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

- (void)createShareView
{
    if (!self.shareView)
    {
        self.shareView = [[ZSTNewShareView alloc] init];
        self.shareView.backgroundColor = [UIColor whiteColor];
        self.shareView.isHasWX = YES;
        self.shareView.isHasQQ = NO;
        self.shareView.isHasWeiBo = NO;
        self.shareView.shareDelegate = self;
        NSInteger heights = 1;
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, heights * 100);
        [self.shareView createShareUI];
        self.shareView.isTodayQQ = YES;
        self.shareView.isTodayWeiBo = YES;
        self.shareView.isTodayWX = YES;
        self.shareView.isTodayWXFriedn = YES;
        [self.shareView checkIsHasShare];
        [self.view addSubview:self.shareView];
    }
}

- (void)buildShareShadowVied
{
    self.shareShadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.shareShadowView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.shareShadowView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.shareShadowView.bounds.size.width, self.shareShadowView.bounds.size.height)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.6;
    [self.shareShadowView addSubview:alphaView];
    self.shareShadowView.hidden = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissNewShareView:)];
    [self.shareShadowView addGestureRecognizer:tap];
}

- (void)showShareView
{
    self.shareShadowView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shareView.frame = CGRectMake(0, HEIGHT - self.shareView.bounds.size.height, WIDTH, self.shareView.bounds.size.height);
        
    }];
}

- (void)showSNSShare
{
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
    
    if (messageClass != nil && [MFMessageComposeViewController canSendText]) {
        
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        
        MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
        NSString *sharebaseurl = self.shareUrl;
        picker.body = [NSString stringWithFormat:NSLocalizedString(@"我在[%@]看到一个不错的商品，你肯定喜欢，赶快来看看吧~地址：%@", nil),appDisplayName,sharebaseurl];
        picker.messageComposeDelegate = self;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    } else {
        
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:"]]];
    }
}

- (void)showDefaultShare
{
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    NSString *shareString = [NSString stringWithFormat:NSLocalizedString(@"我在 [%@] 看到一个不错的商品，你肯定喜欢，赶快来看看吧！", nil),appDisplayName];
    //    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://ci.pmit.cn/d/%@",[ZSTF3Preferences shared].ECECCID]];
    NSURL *URL = [NSURL URLWithString:self.shareUrl];
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[shareString, URL]
                                      applicationActivities:nil];
    
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToFacebook,UIActivityTypePostToWeibo];
    [self.navigationController presentViewController:activityViewController
                                            animated:YES
                                          completion:^{
                                             
                                          }];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismissNewShareView:(UITapGestureRecognizer *)tap
{
    [self animationShareDismiss];
}

- (void)animationShareDismiss
{
    
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, self.shareView.bounds.size.height);
        
    } completion:^(BOOL finished) {
        
        self.shareShadowView.hidden = YES;
        
    }];
}

- (void)dismissShareView
{
    [self animationShareDismiss];
}

- (void)jumpToSomeWeb:(ShoesTabbarBtn *)sender
{
    // 如果退出登录，选中首页；再次登录，再选择其他tabbar要把首页不被选中
    self.selectBtn.selected = NO;
    UIButton *firstBtn = [self.tabbarView viewWithTag:1];
    firstBtn.selected = NO;
    
    self.selectBtn.selected = NO;
    sender.selected = YES;
    self.selectBtn = sender;
    
    switch(sender.tag)
    {
        case 1:
            [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webUrl]]];
            
            self.isGoodsList = NO;
            
            break;
        case 2:
            //[self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.saygogo.cn/mobile/index.php?act=member_cart&op=m_cart_list"]]];
            if (![ShoesUser shareUser].key || [[ShoesUser shareUser].key isEqualToString:@""])
            {
                ShoesMyLoginViewController *loginVC = [[ShoesMyLoginViewController alloc] init];
                loginVC.loginDelegate = self;
                loginVC.isFromLogout = NO;
                self.loginTypeString = @"carlogin";
                UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:loginVC];
                [self presentViewController:navi animated:YES completion:nil];

            }
            else
            {
                [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.fyaiwujia.com/mobile/index.php?act=member_cart&op=m_cart_list"]]];
            }
            
            self.isGoodsList = NO;
            
            break;
        case 3:
            [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.fyaiwujia.com/mobile/index.php?act=goods&op=m_goods_list&id=7"]]];
            
            self.isGoodsList = YES;
            
            break;
        case 4:
            if (![ShoesUser shareUser].key || [[ShoesUser shareUser].key isEqualToString:@""])
            {
                ShoesMyLoginViewController *loginVC = [[ShoesMyLoginViewController alloc] init];
                loginVC.loginDelegate = self;
                loginVC.isFromLogout = NO;
                self.loginTypeString = @"personallogin";
                UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:loginVC];
                [self presentViewController:navi animated:YES completion:nil];
                
            }
            else
            {
                 [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.fyaiwujia.com/mobile/index.php?act=member_index&op=m_personal_center"]]];
            }
            
            self.isGoodsList = NO;
           
            break;
        case 5:
            if (![ShoesUser shareUser].key || [[ShoesUser shareUser].key isEqualToString:@""])
            {
                ShoesMyLoginViewController *loginVC = [[ShoesMyLoginViewController alloc] init];
                loginVC.loginDelegate = self;
                loginVC.isFromLogout = NO;
                self.loginTypeString = @"asklogin";
                UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:loginVC];
                [self presentViewController:navi animated:YES completion:nil];
                
            }
            else
            {
                [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.fyaiwujia.com/mobile/index.php?act=goods&op=askquestion&id=7"]]];
            }
            
            self.isGoodsList = NO;
            
            break;
        default:

            break;
    }
}

- (void)updataFunView:(NSString *)urlString
{
    self.isUpdataFunView = YES;
    
    [self.funView removeFromSuperview];
    
    self.funView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(150), - HeightRate(202), WidthRate(150), HeightRate(202))];
    self.funView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.funView];
    
    CALayer *firstLine = [CALayer layer];
    firstLine.frame = CGRectMake(0, HeightRate(40), WidthRate(150), 0.5);
    firstLine.backgroundColor = [UIColor darkGrayColor].CGColor;
    [self.funView.layer addSublayer:firstLine];
    
    CALayer *secondLine = [CALayer layer];
    secondLine.frame = CGRectMake(0, HeightRate(80.5), WidthRate(150), 0.5);
    secondLine.backgroundColor = [UIColor darkGrayColor].CGColor;
    [self.funView.layer addSublayer:secondLine];
    
    CALayer *thirdLine = [CALayer layer];
    thirdLine.frame = CGRectMake(0, HeightRate(121), WidthRate(150), 0.5);
    thirdLine.backgroundColor = [UIColor darkGrayColor].CGColor;
    [self.funView.layer addSublayer:thirdLine];
    
    CALayer *forthLine = [CALayer layer];
    forthLine.frame = CGRectMake(0, HeightRate(161.5), WidthRate(150), 0.5);
    forthLine.backgroundColor = [UIColor darkGrayColor].CGColor;
    [self.funView.layer addSublayer:forthLine];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_build = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    [self buildFunLB:CGRectMake(0, 0, WidthRate(150), HeightRate(40)) Title:[NSString stringWithFormat:@"当前版本 : %@",app_build] BtnTag:9 FatherView:self.funView];
    [self buildFunLB:CGRectMake(0, HeightRate(40.5), WidthRate(150), HeightRate(40)) Title:@"前进" BtnTag:1000 FatherView:self.funView];
    [self buildFunLB:CGRectMake(0, HeightRate(81), WidthRate(150), HeightRate(40)) Title:@"刷新" BtnTag:1001 FatherView:self.funView];
    
    // 如果是购物车、个人中心、购买咨询 funView 不显示分享
    if ([urlString rangeOfString:@"m_cart_list"].location != NSNotFound || [urlString rangeOfString:@"m_personal_center"].location != NSNotFound || [urlString rangeOfString:@"askquestion"].location != NSNotFound) {
        
        if ([ShoesUser shareUser].key && ![[ShoesUser shareUser].key isEqualToString:@""])
        {
            [self buildFunLB:CGRectMake(0, HeightRate(121.5), WidthRate(150), HeightRate(40)) Title:@"注销" BtnTag:1003 FatherView:self.funView];
            self.funView.frame = CGRectMake(self.funView.frame.origin.x, self.funView.frame.origin.y, self.funView.bounds.size.width, HeightRate(161.5));
        }
    }
    else {
        [self buildFunLB:CGRectMake(0, HeightRate(121.5), WidthRate(150), HeightRate(40)) Title:@"分享" BtnTag:1002 FatherView:self.funView];
        if ([ShoesUser shareUser].key && ![[ShoesUser shareUser].key isEqualToString:@""])
        {
            [self buildFunLB:CGRectMake(0, HeightRate(162), WidthRate(150), HeightRate(40)) Title:@"注销" BtnTag:1003 FatherView:self.funView];
        }
        self.funView.frame = CGRectMake(self.funView.frame.origin.x, self.funView.frame.origin.y, self.funView.bounds.size.width, HeightRate(202));
    }
    
}


- (void)checkUpdate
{
    [[ShoesNetworking defaultNetworking] request:ShoesRequestStatusCheckUpdate WithParameters:@{@"store_id":@(7),@"type":@"ios"} callBackBlock:^(NSDictionary *dic) {
        if ([[dic objectForKey:@"result"] integerValue] == 1)
        {
            NSInteger oldBundleInteger = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] integerValue];
            NSInteger bunldeInteger = [[[dic objectForKey:@"data"] objectForKey:@"var"] integerValue];
            if (bunldeInteger > oldBundleInteger)
            {
                NSString *jumpUrlString = [[dic objectForKey:@"data"] objectForKey:@"url"];
                [self buildUpdateView:bunldeInteger UpdateUrlString:jumpUrlString];
            }
            
        }
            
    } showIndicator:NO];
}

- (void)buildUpdateView:(NSInteger)versionInteger UpdateUrlString:(NSString *)urlString
{
    self.updateUrlString = urlString;
    UIView *darkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    darkView.backgroundColor = [UIColor clearColor];
    self.darkView = darkView;
    
    UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    blackView.backgroundColor = [UIColor blackColor];
    blackView.alpha = 0.6;
    [darkView addSubview:blackView];
    
    UIView *updateView = [[UIView alloc] init];
    updateView.backgroundColor = [UIColor whiteColor];
    updateView.center = CGPointMake(WIDTH / 2, HEIGHT / 2);
    updateView.bounds = (CGRect){CGPointZero,{WidthRate(320),HeightRate(250)}};
    [updateView.layer setCornerRadius:12.0f];
    
    UIImageView *tipsImage = [[UIImageView alloc] initWithFrame:CGRectMake((updateView.frame.size.width - WidthRate(40)) / 2, HeightRate(15), WidthRate(40), WidthRate(40))];
    tipsImage.image = [UIImage imageNamed:@"tipsImage2"];
    tipsImage.contentMode = UIViewContentModeScaleAspectFit;
    [updateView addSubview:tipsImage];
    
    NSString *tempString = [NSString stringWithFormat:@"检测到新版本v%@.0,是否要升级?",@(versionInteger)];
    UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), HeightRate(30) + WidthRate(40), WidthRate(280), HeightRate(100))];
    tipLB.textColor = RGBA(59, 59, 59, 1);
    tipLB.textAlignment = NSTextAlignmentCenter;
    tipLB.font = [UIFont systemFontOfSize:16.0f];
    tipLB.text = tempString;
    [updateView addSubview:tipLB];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(WidthRate(25), HeightRate(145) + WidthRate(40), WidthRate(270), HeightRate(45));
    [sureBtn setTitle:@"我要升级!" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [sureBtn.layer setCornerRadius:16.0f];
    sureBtn.backgroundColor = RGBA(0, 194, 94, 1);
    [sureBtn addTarget:self action:@selector(sureUpdate:) forControlEvents:UIControlEventTouchUpInside];
    [updateView addSubview:sureBtn];
    
//    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    cancelBtn.frame = CGRectMake(WidthRate(25), HeightRate(145) + WidthRate(40), WidthRate(125), HeightRate(45));
//    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
//    [cancelBtn setTitleColor:RGBA(153, 153, 153, 1) forState:UIControlStateNormal];
//    [cancelBtn addTarget:self action:@selector(cancelUpdate:) forControlEvents:UIControlEventTouchUpInside];
//    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
//    [cancelBtn.layer setCornerRadius:16.0f];
//    cancelBtn.backgroundColor = RGBA(241, 241, 241, 1);
//    [updateView addSubview:cancelBtn];
    
    
    // 分隔线（虚线）
    for (NSInteger i = 0; i < 80; i++) {
        
        CALayer *layer1 = [[CALayer alloc] init];
        layer1.frame = CGRectMake(4*i, HeightRate(130) + WidthRate(40), 2, 1);
        layer1.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [updateView.layer addSublayer:layer1];
        updateView.clipsToBounds = YES;
    }
    
    [darkView addSubview:updateView];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [app.window addSubview:darkView];
    
}

- (void)sureUpdate:(UIButton *)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.updateUrlString]];
    [self exitApplication];
}

- (void)cancelUpdate:(UIButton *)sender
{
    [self.darkView removeFromSuperview];
}

- (void)exitApplication {
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    UIWindow *window = app.window;
    
    [UIView animateWithDuration:1.0f animations:^{
        window.alpha = 0;
        window.frame = CGRectMake(0, window.bounds.size.width, 0, 0);
    } completion:^(BOOL finished) {
        exit(0);
    }];
    
}

- (void)buildRemindView
{
    self.remindDarkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.remindDarkView.backgroundColor = [UIColor clearColor];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [app.window addSubview:self.remindDarkView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.6;
    [self.remindDarkView addSubview:alphaView];
    
    self.alertView = [[UIView alloc] init];
    self.alertView.backgroundColor = [UIColor whiteColor];
    self.alertView.center = CGPointMake(WIDTH / 2, HEIGHT / 2);
    self.alertView.bounds = (CGRect){CGPointZero,{WidthRate(320),HeightRate(250)}};
    [self.alertView.layer setCornerRadius:12.0f];
    
    UIImageView *tipsImage = [[UIImageView alloc] initWithFrame:CGRectMake((self.alertView.frame.size.width - WidthRate(40)) / 2, HeightRate(15), WidthRate(40), WidthRate(40))];
    tipsImage.image = [UIImage imageNamed:@"tipsImage3"];
    tipsImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.alertView addSubview:tipsImage];
    
    NSString *tempString = @" 亲，先注册后分享，会有丰厚红利喔!";
    //CGSize countSize = [tempString boundingRectWithSize:CGSizeMake(MAXFLOAT, 30) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:20.0f]} context:nil].size;
    
    UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), HeightRate(30) + WidthRate(40), WidthRate(280), HeightRate(100))];
    tipLB.textColor = RGBA(59, 59, 59, 1);
    tipLB.numberOfLines = 0;
    tipLB.textAlignment = NSTextAlignmentCenter;
    tipLB.font = [UIFont systemFontOfSize:16.0f];
    tipLB.text = tempString;
    [self.alertView addSubview:tipLB];
    
    [self.remindDarkView addSubview:self.alertView];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(WidthRate(170), HeightRate(145) + WidthRate(40), WidthRate(125), HeightRate(45));
    [sureBtn setTitle:@"立即登录" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [sureBtn.layer setCornerRadius:16.0f];
    sureBtn.backgroundColor = RGBA(0, 194, 94, 1);
    [sureBtn addTarget:self action:@selector(sureLogin:) forControlEvents:UIControlEventTouchUpInside];
    [self.alertView addSubview:sureBtn];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(WidthRate(25), HeightRate(145) + WidthRate(40), WidthRate(125), HeightRate(45));
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:RGBA(153, 153, 153, 1) forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelLogin:) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [cancelBtn.layer setCornerRadius:16.0f];
    cancelBtn.backgroundColor = RGBA(241, 241, 241, 1);
    [self.alertView addSubview:cancelBtn];
    
    
    // 分隔线（虚线）
    for (NSInteger i = 0; i < 80; i++) {
        
        CALayer *layer1 = [[CALayer alloc] init];
        layer1.frame = CGRectMake(4*i, HeightRate(130) + WidthRate(40), 2, 1);
        layer1.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [self.alertView.layer addSublayer:layer1];
        self.alertView.clipsToBounds = YES;
    }
}

- (void)sureLogin:(UIButton *)sender
{
    [self.alertView removeFromSuperview];
    self.alertView = nil;
    [self.remindDarkView removeFromSuperview];
    self.remindDarkView = nil;
    
    self.loginTypeString = @"shareLogin";
    
    ShoesMyLoginViewController *loginVC = [[ShoesMyLoginViewController alloc] init];
    loginVC.loginDelegate = self;
    loginVC.isFromLogout = NO;
    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:loginVC];
    [self presentViewController:navi animated:YES completion:^{
        
    }];

}

- (void)cancelLogin:(UIButton *)sender
{
    [self.alertView removeFromSuperview];
    self.alertView = nil;
    [self.remindDarkView removeFromSuperview];
    self.remindDarkView = nil;
//    [self showShareView];
}

- (void)reqeustForWXPay:(NSString *)orderId Type:(NSString *)typeString
{
    NSString *urlString = @"";
    if ([typeString isEqualToString:@"order"])
    {
        urlString = [NSString stringWithFormat:@"http://www.fyaiwujia.com/mobile/index.php?act=member_payment&op=%@&pay_sn=%@",@"wechatpay",orderId];
    }
    else if ([typeString isEqualToString:@"authentication"])
    {
        urlString = [NSString stringWithFormat:@"http://www.fyaiwujia.com/mobile/index.php?act=member_payment&op=%@&pay_sn=%@",@"wechatpay_distribution",orderId];
    }
    else if ([typeString isEqualToString:@"recharge"])
    {
        urlString = [NSString stringWithFormat:@"http://www.fyaiwujia.com/mobile/index.php?act=member_payment&op=%@&pay_sn=%@",@"pd_wechatpay_topay",orderId];
    }
    
    
    [[ShoesNetworking defaultNetworking] requestString:urlString WithParameters:nil callBackBlock:^(NSDictionary *dic) {
        
        if ([[dic safeObjectForKey:@"data"] isKindOfClass:[NSDictionary class]])
        {
            [self weixinPayWithRequsetData:[dic safeObjectForKey:@"data"]];
        }
        
        
    } showIndicator:YES];
    
}

#pragma mark - 微信支付
- (void)weixinPayWithRequsetData:(NSDictionary *)dict
{
    if(dict != nil){
        NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
        PayReq *request = [[PayReq alloc] init];
        request.openID = wechatKey;
        request.partnerId = [dict objectForKey:@"partnerid"];
        request.prepayId = [dict objectForKey:@"prepayid"];
        request.package = @"Sign=WXPay";
        request.nonceStr = [dict objectForKey:@"noncestr"];
        request.timeStamp = stamp.intValue;
        request.sign= [dict objectForKey:@"sign"];
        [WXApi sendReq:request];
        
        
    }else{
        //        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微信支付提示" message:[NSString stringWithFormat:@"支付失败:%@",[dict objectNullForKey:@"retmsg"]] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        //        [alertView show];
    }
}

- (void)loginHasCancel
{
    self.selectBtn.selected = NO;
    ShoesTabbarBtn *homeBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:1];
    homeBtn.selected = YES;
    self.selectBtn = homeBtn;
    
    [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webUrl]]];
}

- (void)loginDidSuccess:(NSNotification *)notification
{
    [self updataFunView:@""];
}

- (void)weixinPayCallBack:(NSNotification *)notification
{
    [self.shoesWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.fyaiwujia.com/mobile/index.php?act=member_index&op=m_personal_center"]]];
}

#pragma mark - webViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"urlString --> %@",request.URL);
    
    NSString *urlString = [NSString stringWithFormat:@"%@",request.URL];
    
    if ([urlString rangeOfString:@"m_cart_list"].length > 0)
    {
        self.selectBtn.selected = NO;
        ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:2];
        firstBtn.selected = YES;
        self.selectBtn = firstBtn;
        if (![ShoesUser shareUser].key || [[ShoesUser shareUser].key isEqualToString:@""])
        {
            ShoesMyLoginViewController *loginVC = [[ShoesMyLoginViewController alloc] init];
            loginVC.loginDelegate = self;
            loginVC.isFromLogout = NO;
            self.loginTypeString = @"carlogin";
            UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:loginVC];
            [self presentViewController:navi animated:YES completion:nil];
            return NO;
        }
        
        self.tabbarView.hidden = NO;
        self.shoesWebView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - HeightRate(55));
        
        // 改变 funView
        [self updataFunView:urlString];
    }
    else if ([urlString rangeOfString:@"m_goods_list"].length > 0)
    {
        self.selectBtn.selected = NO;
        ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:3];
        firstBtn.selected = YES;
        self.selectBtn = firstBtn;
        
        if (self.isUpdataFunView) {
            [self updataFunView:urlString];
        }
        
        self.tabbarView.hidden = NO;
        self.shoesWebView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - HeightRate(55));
    }
    else if ([urlString rangeOfString:@"m_personal_center"].length > 0)
    {
        self.selectBtn.selected = NO;
        ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:4];
        firstBtn.selected = YES;
        self.selectBtn = firstBtn;
        
        self.tabbarView.hidden = NO;
        self.shoesWebView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - HeightRate(55));
        
        [self updataFunView:urlString];
    }
    else if ([urlString rangeOfString:@"askquestion"].length > 0)
    {
        self.selectBtn.selected = NO;
        ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:5];
        firstBtn.selected = YES;
        self.selectBtn = firstBtn;
        
        self.tabbarView.hidden = NO;
        self.shoesWebView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - HeightRate(55));
        
        [self updataFunView:urlString];
    }
    else if ([urlString rangeOfString:@"m_store"].length > 0)
    {
        self.selectBtn.selected = NO;
        ShoesTabbarBtn *firstBtn = (ShoesTabbarBtn *)[self.tabbarView viewWithTag:1];
        firstBtn.selected = YES;
        self.selectBtn = firstBtn;
        
        self.tabbarView.hidden = NO;
        self.shoesWebView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - HeightRate(55));
        
        if (self.isUpdataFunView) {
            [self updataFunView:urlString];
        }
    }
    else if ([urlString rangeOfString:@"passOrderId"].length > 0)
    {
        NSInteger andLength = [urlString rangeOfString:@"&"].location;
        NSString *orderId = [urlString substringWithRange:NSMakeRange(@"ios://passOrderId?orderId=".length, andLength - @"ios://passOrderId?orderId=".length)];
        NSInteger typeLocation = [urlString rangeOfString:@"&type="].location + @"&type=".length;
        NSString *typeString = [urlString substringFromIndex:typeLocation];
        
        [self reqeustForWXPay:orderId Type:typeString];
        
        
        return NO;
    }
    else
    {
        self.tabbarView.hidden = YES;
        self.shoesWebView.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
    }
    
    self.shareUrl = [NSString stringWithFormat:@"%@", request.URL];
    self.shareView.shareUrlString = self.shareUrl;
    self.shareView.shareString = self.shareUrl;
    
    if (self.isFunOpen)
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.funView.frame = CGRectMake(WIDTH - WidthRate(150), - HeightRate(161.5), WidthRate(150), HeightRate(161.5));
            
        } completion:^(BOOL finished) {
            
            self.isFunOpen = NO;
        }];
    }
    
    
    //NSLog(@"HHHHHHH === %@", urlString);
    if ([urlString rangeOfString:@"act=login&op=m_login"].length > 0)
    {
        ShoesMyLoginViewController *loginVC = [[ShoesMyLoginViewController alloc] init];
        loginVC.loginDelegate = self;
        loginVC.isFromLogout = NO;
        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:loginVC];
        [self presentViewController:navi animated:YES completion:nil];
        
        return NO;
    }
    
    return YES;
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    //    self.shadowView.hidden = NO;
    //    [self.activityIndicator startAnimating];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if ([webView canGoBack])
    {
        self.backBtn.hidden = NO;
    }
    else
    {
        self.backBtn.hidden = YES;
    }
    
    self.shareTitle = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.shareView.shareTitleString = self.shareTitle;
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    //    [self.activityIndicator stopAnimating];
    //    self.shadowView.hidden = YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //    [self.activityIndicator stopAnimating];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.shadowView.hidden = YES;
    
    NSLog(@"error -->%@",error);
    if ([error code] == NSURLErrorCancelled)
    {
        return;
    }
    else
    {
        ZSTWebFailureViewController *failureVC = [[ZSTWebFailureViewController alloc] init];
        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:failureVC];
        [self.navigationController presentViewController:navi animated:YES completion:nil];
    }
    
}

#pragma mark - scrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.isFunOpen)
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.funView.frame = CGRectMake(WIDTH - WidthRate(150), - HeightRate(161.5), WidthRate(150), HeightRate(202));
            
        } completion:^(BOOL finished) {
            
            self.isFunOpen = NO;
        }];
    }
}



#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
//    progress = progress < 0.2 ? 0.2 : progress;
    [_progressView setProgress:progress animated:YES];
}


@end
