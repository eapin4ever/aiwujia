//
//  ShoesAlertView.h
//  shoes
//
//  Created by pmit on 15/9/10.
//  Copyright (c) 2015年 saygogo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ShoesWarningType)
{
    ShoesWarningTypeWrong,
    ShoesWarningTypeRight,
    ShoesWarningTypeWarning
};

@interface ShoesAlertView : UIView

@property (strong,nonatomic) UIImageView *warningTypeIV;
@property (strong,nonatomic) UILabel *titleLB;
@property (strong,nonatomic) UIButton *actionBtn;

+ (ShoesAlertView *)shareAlertView;

- (void)showInView:(UIView *)showView WaringType:(ShoesWarningType)warningType WaringTitle:(NSString *)warningTitle IsBtnShow:(BOOL)isBtnShow BtnTitle:(NSString *)btnTitle BtnTarget:(id)target BtnSEL:(SEL)seletor;

- (void)hideTip:(UIView *)showView;

@end
