//
//  ShoesAlertView.m
//  shoes
//
//  Created by pmit on 15/9/10.
//  Copyright (c) 2015年 saygogo. All rights reserved.
//

#import "ShoesAlertView.h"

@implementation ShoesAlertView

static ShoesAlertView *_instance;

+ (ShoesAlertView *)shareAlertView
{
    if (!_instance)
    {
        _instance = [[ShoesAlertView alloc] initWithFrame:CGRectMake(0, 0, WidthRate(300), HeightRate(200))];
        [_instance createUI];
        _instance.alpha = 0;
        UIWindow *windom = [[UIApplication sharedApplication].windows firstObject];
        [windom addSubview:_instance];
    }
    
    return _instance;
}

- (void)createUI
{
    UIImageView *warningBackIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _instance.bounds.size.width, _instance.bounds.size.height)];
    warningBackIV.image = [UIImage imageNamed:@"warningBg"];
    warningBackIV.contentMode = UIViewContentModeScaleAspectFit;
    warningBackIV.userInteractionEnabled = YES;
    [_instance addSubview:warningBackIV];
    
    self.warningTypeIV = [[UIImageView alloc] init];
    self.warningTypeIV.center = CGPointMake(warningBackIV.bounds.size.width / 2, HeightRate(40));
    self.warningTypeIV.bounds = (CGRect){CGPointZero,{WidthRate(40),WidthRate(40)}};
    self.warningTypeIV.contentMode = UIViewContentModeScaleAspectFit;
    [warningBackIV addSubview:self.warningTypeIV];
    
    self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.warningTypeIV.frame) + HeightRate(10), warningBackIV.bounds.size.width, HeightRate(60))];
    self.titleLB.font = [UIFont systemFontOfSize:14.0f];
    self.titleLB.textColor = RGBA(57, 57, 57, 1);
//    self.titleLB.text = @"恭喜\n\n您已经注册成功";
    self.titleLB.numberOfLines = 0;
    self.titleLB.textAlignment = NSTextAlignmentCenter;
    [warningBackIV addSubview:self.titleLB];
    
    self.actionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionBtn.center = CGPointMake(warningBackIV.bounds.size.width / 2, HeightRate(170));
    self.actionBtn.bounds = (CGRect){CGPointZero,{WidthRate(120),HeightRate(40)}};
    [self.actionBtn setBackgroundImage:[UIImage imageNamed:@"btnBg"] forState:UIControlStateNormal];
    [warningBackIV addSubview:self.actionBtn];
    
    
}

- (void)showInView:(UIView *)showView WaringType:(ShoesWarningType)warningType WaringTitle:(NSString *)warningTitle IsBtnShow:(BOOL)isBtnShow BtnTitle:(NSString *)btnTitle BtnTarget:(id)target BtnSEL:(SEL)seletor
{
    _instance.center = CGPointMake(WIDTH / 2, HEIGHT / 2);
    NSString *imageName = @"";
    switch (warningType)
    {
        case ShoesWarningTypeRight:
            imageName = @"tick";
            break;
        case ShoesWarningTypeWarning:
            imageName = @"wow";
            break;
        case ShoesWarningTypeWrong:
            imageName = @"wrong";
            break;
            
        default:
            break;
    }
    
    self.warningTypeIV.image = [UIImage imageNamed:imageName];
    self.titleLB.text = warningTitle;
    
    if (!isBtnShow)
    {
        _instance.actionBtn.hidden = YES;
        [UIView animateWithDuration:0.6 animations:^{
            _instance.alpha = 1.0f;
        } completion:^(BOOL finished) {
            
        }];
        
    }
    else
    {
        _instance.actionBtn.hidden = NO;
        [self.actionBtn removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
        [self.actionBtn setTitle:btnTitle forState:UIControlStateNormal];
        [self.actionBtn addTarget:target action:seletor forControlEvents:UIControlEventTouchUpInside];
        [UIView animateWithDuration:0.6 animations:^{
            _instance.alpha = 1.0f;
        } completion:^(BOOL finished) {
        
        }];
    }
}

- (void)hideTip:(UIView *)showView
{
    [UIView animateWithDuration:1.5f animations:^{
        _instance.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

@end
