//
//  CustomerTextField.m
//  shoes
//
//  Created by ZhangEapin on 15/9/20.
//  Copyright © 2015年 saygogo. All rights reserved.
//

#import "CustomerTextField.h"

@implementation CustomerTextField

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    return CGRectMake(0, HeightRate(2.5), self.bounds.size.width, self.bounds.size.height - HeightRate(5));
}

@end
