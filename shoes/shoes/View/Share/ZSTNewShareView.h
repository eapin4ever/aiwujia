//
//  ZSTNewShareView.h
//  F3
//
//  Created by pmit on 15/8/27.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WXApi.h"
#import "WeiboSDK.h"
#import <TencentOpenAPI/TencentApiInterface.h>
#import <TencentOpenAPI/TencentMessageObject.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <MessageUI/MessageUI.h>

@protocol ZSTNewShareViewDelegate <NSObject>

- (void)showDefaultShare;
- (void)showSNSShare;
- (void)dismissShareView;

@end

#define WEIXINTAG 1
#define WEIXINFRIENDTAG 2
#define WEIBOTAG 3
#define QQTAG 4
#define SMSTAG 5
#define MORETAG 6

@interface ZSTNewShareView : UIView

@property (assign,nonatomic) NSInteger shareCount;
@property (assign,nonatomic) BOOL isHasWX;
@property (assign,nonatomic) BOOL isHasQQ;
@property (assign,nonatomic) BOOL isHasWeiBo;
@property (assign,nonatomic) NSInteger wxSharePoint;
@property (assign,nonatomic) NSInteger wxFriendSharePoint;
@property (assign,nonatomic) NSInteger weiboSharePoint;
@property (assign,nonatomic) NSInteger qqSharePoint;
@property (assign,nonatomic) NSInteger smsSharePoint;
@property (assign,nonatomic) BOOL isTodayWX;
@property (assign,nonatomic) BOOL isTodayWXFriedn;
@property (assign,nonatomic) BOOL isTodayWeiBo;
@property (assign,nonatomic) BOOL isTodayQQ;
@property (weak,nonatomic) id<ZSTNewShareViewDelegate> shareDelegate;
@property (assign,nonatomic) BOOL isHasImage;
@property (copy,nonatomic) NSString *shareString;
@property (copy,nonatomic) NSString *imgUrlString;
@property (copy,nonatomic) NSString *shareUrlString;
@property (strong,nonatomic) UIButton *snsBtn;
@property (copy,nonatomic) NSString *shareTitleString;

- (void)createShareUI;
- (void)checkIsHasShare;

@end
