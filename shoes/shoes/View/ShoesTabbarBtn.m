//
//  ShoesTabbarBtn.m
//  shoes
//
//  Created by ZhangEapin on 15/9/23.
//  Copyright © 2015年 saygogo. All rights reserved.
//

#import "ShoesTabbarBtn.h"

@implementation ShoesTabbarBtn

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, contentRect.size.height * 0.7, contentRect.size.width, contentRect.size.height * 0.3 - HeightRate(2));
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, 5, contentRect.size.width, contentRect.size.height * 0.6 - 5);
}

@end
