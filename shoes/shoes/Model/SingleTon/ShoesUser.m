//
//  ShoesUser.m
//  shoes
//
//  Created by pmit on 15/9/10.
//  Copyright (c) 2015年 saygogo. All rights reserved.
//

#import "ShoesUser.h"

@implementation ShoesUser

static ShoesUser *_instance;

+ (ShoesUser *)shareUser
{
    if (!_instance)
    {
        _instance = [[ShoesUser alloc] init];
    }
    
    return _instance;
}

@end
