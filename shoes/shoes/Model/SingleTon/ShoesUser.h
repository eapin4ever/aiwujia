//
//  ShoesUser.h
//  shoes
//
//  Created by pmit on 15/9/10.
//  Copyright (c) 2015年 saygogo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShoesUser : NSObject

@property (copy,nonatomic) NSString *userName;
@property (copy,nonatomic) NSString *key;
@property (copy,nonatomic) NSString *memberId;

+ (ShoesUser *)shareUser;

@end
