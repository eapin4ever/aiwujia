//
//  ShoesCheckUtil.h
//  shoes
//
//  Created by pmit on 15/9/10.
//  Copyright (c) 2015年 saygogo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShoesCheckUtil : NSObject

+ (BOOL)validateMobile:(NSString *)mobileNum;

@end
