//
//  NSDictionary+ShoesUtil.m
//  shoes
//
//  Created by pmit on 15/9/10.
//  Copyright (c) 2015年 saygogo. All rights reserved.
//

#import "NSDictionary+ShoesUtil.h"

@implementation NSDictionary (ShoesUtil)

- (id)safeObjectForKey:(id)aKey
{
    id anObject = [self objectForKey:aKey];
    if (anObject == [NSNull null]) {
        anObject = nil;
    }
    return anObject;
}

@end
