//
//  ShoesNetworking.m
//  shoes
//
//  Created by pmit on 15/9/9.
//  Copyright (c) 2015年 saygogo. All rights reserved.
//

#import "ShoesNetworking.h"
#import <AFNetworking.h>
#import <AFHTTPRequestOperationManager.h>

//#define ip(address) [NSString stringWithFormat:@"http://www.saygogo.cn/mobile/%@",address]
#define ip(address) [NSString stringWithFormat:@"http://www.fyaiwujia.com/mobile/%@",address]

@interface ShoesNetworking()

@property(nonatomic,strong) AFHTTPRequestOperationManager *manager;

@end

static ShoesNetworking *_networking;

@implementation ShoesNetworking

+ (ShoesNetworking *)defaultNetworking
{
    if(!_networking)
    {
        _networking = [[ShoesNetworking alloc] init];
    }
    
    return _networking;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.manager = [AFHTTPRequestOperationManager manager];
        self.manager.securityPolicy.allowInvalidCertificates = YES;
        //   去掉响应头的智能转换
        [self.manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
        
    }
    return self;
}

- (void)request:(ShoesRequestStatus)requestState WithParameters:(id)parameter callBackBlock:(callBackDicBlock)callback showIndicator:(BOOL)show
{
    NSString *requestUrl = @"";
    
    switch (requestState) {
        case ShoesRequestStatusLogin:
            requestUrl = ip(@"index.php?act=login&op=m_loginajax&store_id=7");
            break;
        case ShoesRequestStatusRegister:
            requestUrl = ip(@"index.php?act=login&op=m_registerajax&store_id=7");
            break;
        case ShoesRequestStatusLogout:
            requestUrl = ip(@"index.php?act=logout&op=m_logoutajax&store_id=7");
            break;
        case ShoesRequestStatusGetSMS:
            requestUrl = ip(@"index.php?act=login&op=m_getVerifyCode&store_id=7");
            break;
        case ShoesRequestStatusCheckSMS:
            requestUrl = ip(@"index.php?act=login&op=m_checkVerifyCode&store_id=7");
            break;
        case ShoesRequestStatusThirdLogin:
            requestUrl = ip(@"index.php?act=login&op=m_loginByToken&store_id=7");
            break;
        case ShoesRequestStatusFindPass:
            requestUrl = ip(@"index.php?act=login&op=m_findPassword&store_id=7");
            break;
        case ShoesRequestStatusCheckUpdate:
            requestUrl = ip(@"index.php?act=mobile_update&op=index&store_id=7");
            break;
        default:
            break;
    }
    
    
    __block NSDictionary *resultDic;
    
    NSMutableDictionary *param = [parameter mutableCopy];
    
    [param setValue:@"wap" forKey:@"client"];
    
    [self.manager POST:requestUrl parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (responseObject)
        {
            resultDic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            
            NSLog(@"resultDic --> %@",resultDic);
            
            callback(resultDic);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //NSLog(@"error === %@", error);
    }];
   
}

- (void)requestString:(NSString *)requestUrl WithParameters:(id)parameter callBackBlock:(callBackDicBlock)callback showIndicator:(BOOL)show
{
    
    __block NSDictionary *resultDic;
    
    NSMutableDictionary *param = [parameter mutableCopy];
    
    [param setValue:@"wap" forKey:@"client"];
    
    [self.manager POST:requestUrl parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (responseObject)
        {
            resultDic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            
            NSLog(@"resultDic --> %@",resultDic);
            
            callback(resultDic);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //NSLog(@"error === %@", error);
    }];
    
}

@end
