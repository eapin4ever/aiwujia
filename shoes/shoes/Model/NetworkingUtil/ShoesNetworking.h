//
//  ShoesNetworking.h
//  shoes
//
//  Created by pmit on 15/9/9.
//  Copyright (c) 2015年 saygogo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^callBackDicBlock)(NSDictionary *dic);
typedef void(^connectRetry)();

typedef NS_ENUM(NSInteger, ShoesRequestStatus)
{
    ShoesRequestStatusLogin,
    ShoesRequestStatusRegister,
    ShoesRequestStatusLogout,
    ShoesRequestStatusGetSMS,
    ShoesRequestStatusCheckSMS,
    ShoesRequestStatusThirdLogin,
    ShoesRequestStatusFindPass,
    ShoesRequestStatusCheckUpdate,
    ShoesRequestStatusOrderGoToWXPay
};

@interface ShoesNetworking : NSObject

+ (ShoesNetworking *)defaultNetworking;
- (void)request:(ShoesRequestStatus)requestState WithParameters:(id)parameter callBackBlock:(callBackDicBlock)callback showIndicator:(BOOL)show;
- (void)requestString:(NSString *)requestUrl WithParameters:(id)parameter callBackBlock:(callBackDicBlock)callback showIndicator:(BOOL)show;


@end
