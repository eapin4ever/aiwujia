//
//  AppDelegate.h
//  shoes
//
//  Created by pmit on 15/9/9.
//  Copyright (c) 2015年 saygogo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (copy,nonatomic) NSString *wbtoken;
@property (strong, nonatomic) NSString *wbCurrentUserID;
@property (strong, nonatomic) NSString *wbRefreshToken;

- (void)sendAuthRequest;
- (void)sendWeixinPeople;


@end

